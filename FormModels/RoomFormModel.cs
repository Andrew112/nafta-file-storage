﻿using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.FormModels
{
    public class RoomFormModel
    {
        public int? Id { get; set; }
        public string Professor { get; set; }
        public string Subject { get; set; }
        public string[] Groups { get; set; }
        public Assignment[] Assignments { get; set; }
        public int[] Files { get; set; }

        public Room Room
        {
            get => new Room
            {
                Id = Id ?? 0
            };
        }
    }
}
