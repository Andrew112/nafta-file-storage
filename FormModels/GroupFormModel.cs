﻿using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.FormModels
{
    public class GroupFormModel
    {
        public int? Id { get; set; }
        [Required, MinLength(4)]
        public string Name { get; set; }
        public string Monitor { get; set; }
        public string[] Students { get; set; }

        public Group Group
        {
            get
            {
                var group = new Group()
                {
                    Name = Name
                };
                group.Id = Id ?? group.Id;
                return group;
            }
        }
    }
}
