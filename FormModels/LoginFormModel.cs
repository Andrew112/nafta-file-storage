﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.FormModels
{
    public class LoginFormModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
