﻿import { UserPermissions } from '../helpers/userPermissions';
import { Assignment } from './assignment'
import { Group } from './group'
import { Subject } from './subject'
import { StoredFile } from './file'
import { User } from './user'

export class Room {
    id: number;
    description: string;
    permissions: UserPermissions;
    subject: Subject;
    professor: User;
    assignments: Assignment[];
    files: StoredFile[];
    groups: Group[];
}