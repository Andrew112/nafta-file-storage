﻿import { UserAssignment } from './userAssignment'

export class Assignment {
    id: number;
    name: string;
    description: string;
    deadline: Date;

    userAssignments: UserAssignment[];
}