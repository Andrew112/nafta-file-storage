﻿import { Assignment } from './assignment'
import { StoredFile } from './file'
import { User } from './user'


export class UserAssignment {
    assignment: Assignment;
    file: StoredFile;
    user: User;
}