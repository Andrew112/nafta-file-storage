﻿import { UserPermissions } from '../helpers/userPermissions';

import { Group } from './group';
import { Room } from './room';

export class User {
    id: number;
    userName: string;
    firstName: string;
    lastName: string;
    fullName: string;
    permissions: UserPermissions;
    email: string;
    emailConfirmed: boolean;
    phoneNumber: string;

    static sortFunc(a: User, b: User) {
        let aFull = a.lastName + a.firstName;
        let bFull = b.lastName + b.firstName;
        return aFull.localeCompare(bFull);;
    }
}