﻿export class StoredFile {
    id: number;
    name: string;
    size: number;
}