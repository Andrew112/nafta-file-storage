"use strict";
//  A subject model
var Subject = (function () {
    function Subject(id, name, description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
    return Subject;
}());
exports.Subject = Subject;
//# sourceMappingURL=subject.js.map