﻿
import { Component } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';


@Component({
    selector: 'users-list',
    templateUrl: 'usersList.component.html',
    providers: [UserService]
})
export class UsersListComponent {
    private users: User[];
    private errorMessage: string;
    private restUrl: string = "/api/Users/";

    constructor(private userService: UserService) {
        this.getUsers();
    }

    private getUsers() {
        this.userService.getUsers()
            .subscribe(
            x => this.users = x.json(),
            error => {
                this.errorMessage = "Не вдалось отримати дані";
                console.log("error: " + error);
            }
            );
    }
}
