﻿import { Component, Input } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Component({
    selector: 'file-download',
    templateUrl: 'fileDownload.component.html'
})
export class FileDownloadComponent {
    @Input() path: string;

    constructor(private http: Http) {
    }

    onClick() {
        let $path = "download/" + this.path;
        window.open($path);
    }

    downloadFile(data: Response) {
        var blob = new Blob([data.text()], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        window.open(url);
    }
}
