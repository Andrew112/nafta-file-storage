﻿import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm, FormsModule } from '@angular/forms';

import { ProgressHttp, Progress } from "angular-progress-http";

import { Observable } from 'rxjs/Observable';

import { FileUploadData } from './fileUploadData';
import { AuthService } from '../../services/auth.service';
import { StoredFile } from '../../models/file';

@Component({
    selector: 'multiple-file-uploader',
    templateUrl: 'multipleFileUploader.component.html',
})
export class MultipleFileUploaderComponent {
    private pendingFiles: Array<File> = [];
    private uploadedFiles: Array<StoredFile> = [];
    //  Any files currently uploading?
    private isUploading: boolean = false;
    private uploadProgress: number;

    @Output() changed = new EventEmitter<FileUploadData>();
    @Input() initiallyUploaded: StoredFile[];

    constructor(private http: ProgressHttp) {
    }

    ngOnInit() {
        if (this.initiallyUploaded) {
            this.uploadedFiles = this.initiallyUploaded;
        }
    }

    //  Emits changed event and passes data about uploaded and pending files
    updateChanged() {
        this.changed.emit({ uploadedFiles: this.uploadedFiles, hasPending: this.pendingFiles.length > 0 });
    }

    //  Is called when user adds files to pending
    onChange(event) {
        //  Adding all files from file input to pendingFIles array
        for (let i = 0; i < event.srcElement.files.length; i++)
            this.pendingFiles.push(event.srcElement.files[i]);
        this.updateChanged();
    }

    //  Removes item from pending files by index
    removePending(index) {
        this.pendingFiles.splice(index, 1);
        this.updateChanged();
    }

    //  Removes item from uploaded files by index
    removeUploaded(index) {
        this.uploadedFiles.splice(index, 1);
        this.updateChanged();
    }

    //  Is called on form submitted
    onSubmit(fileForm: NgForm) {
        if (this.pendingFiles.length == 0)
            return;

        let formData: FormData = new FormData();
        //  Appending all pending files to formData
        for (let i = 0; i < this.pendingFiles.length; i++) {
            formData.append("files", this.pendingFiles[i], this.pendingFiles[i].name);
        }
        this.isUploading = true;
        //  Sending the request to server
        this.http
            .withUploadProgressListener(progress => this.uploadProgress = progress.percentage)
            .post("api/Files", formData, AuthService.getAuthorizationOptions()).subscribe(
            x => {
                //  Adding all successfully uploaded files to uploadedFiles array
                this.uploadedFiles = this.uploadedFiles.concat(x.json());
                //  No more files are pending
                this.pendingFiles = [];
                //  Sending message to the parent
                this.updateChanged();
            },
            //  TODO act somehow here
            error => console.log(error),
            //  Files aren't uploaded anymore
            () => this.isUploading = false
            );
    }
}
