﻿import { StoredFile } from '../../models/file';

export interface FileUploadData {
    uploadedFiles: StoredFile[];
    hasPending: boolean;
}
