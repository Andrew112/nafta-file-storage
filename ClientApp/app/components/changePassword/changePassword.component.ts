﻿import { Component, Input } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { AuthService } from '../../services/auth.service'
import { UserService } from '../../services/user.service'


import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'change-password',
    templateUrl: 'changePassword.component.html'
})
export class ChangePasswordComponent {
    @Input() id: number;

    private formState: FormBehaviour = new FormBehaviour();
    private changePasswordForm: FormGroup;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private http: Http) {
    }

    ngOnInit() {
        this.changePasswordForm = this.formBuilder.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[A-z0-9_]+$")]],
            newPasswordRepeat: ['', [Validators.required]]
        });
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...")

        if (formData.value["newPassword"] != formData.value["newPasswordRepeat"]) {
            this.formState.changeState(FormStates.error, "Паролі не співпадають");
        } else {
            this.userService.changePassword(formData.value, this.id).subscribe(
                x => {
                    this.formState.changeState(FormStates.success, "Пароль успішно змінено");
                },
                error => {
                    this.onSubmitFailed(error);
                }
            )
        }
    }

    private onSubmitFailed(error) {
        switch (error.status) {
            case 403:
                this.formState.changeState(FormStates.error, "Ви ввели некоректні дані");
                break;
            case 0:
                this.formState.changeState(FormStates.error, "Відсутній зв'язок з сервером");
                break;
            default:
                this.formState.changeState(FormStates.error, "Невідома помилка");
        }
    }
}
