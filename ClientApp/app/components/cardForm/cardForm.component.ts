﻿
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'card-form',
    templateUrl: 'cardForm.component.html'
})
export class CardFormComponent {
    //  Is emitted when close button is pressed
    @Output() closed: EventEmitter<any> = new EventEmitter();
    //  Title for the card
    @Input() title: string;
    @Input() formGroup: FormGroup;

    constructor() {
    }

    private close() {
        this.closed.emit();
    }
}
