﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'tag',
    templateUrl: 'tag.component.html'
})
export class TagComponent {
    @Input() value: string;

    @Output() deleted: EventEmitter<string> = new EventEmitter();

    protected close() {
        this.deleted.emit(this.value);
    }
}
