﻿import { Component } from '@angular/core';

import { RoomService } from '../../services/room.service';
import {Room} from '../../models/room'

@Component({
    selector: 'rooms-list',
    templateUrl: 'roomsList.component.html',
    providers: [RoomService]
})
export class RoomsListComponent {
    public rooms: Room[];

    constructor(private roomService: RoomService) {
        this.getRooms();
    }

    getRooms() {
        //  TODO: implement paging
        this.roomService.getRooms(10, 0).subscribe(
            x => { // success
                this.rooms = x.json();
            },
            error => { // error
                console.log("error: " + error);
            }
        )
    }

    fullName(room: Room): string {
        let _fullName: string = room.subject.name + " (";
        for (var i = 0; i < room.groups.length; i++) {
            _fullName += room.groups[i].name + (i == room.groups.length - 1 ? "" : ",");
        }
        _fullName += ")"
        return _fullName;
    }
}
