﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'file-icon',
    templateUrl: 'fileIcon.component.html'
})
export class FileIconComponent {
    @Input() private extension: string;
    private iconClass: string;

    ngOnChanges() {
        switch (this.extension) {
            case '.doc':
            case '.docx':
                this.iconClass = 'fa-file-word-o';
                break;
            case '.pdf':
                this.iconClass = 'fa-file-pdf-o';
                break;
            default:
                this.iconClass = 'fa-file';
                break;
        }
    }
}
