﻿import { Component } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { AuthService } from '../../services/auth.service'
import { UserRolesSercice } from '../../services/userRoles.service'

import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
    selector: 'user-form',
    templateUrl: 'userForm.component.html',
    providers: [FormBuilder]
})
export class UserFormComponent {
    private userForm: FormGroup;

    private userRoles: any;
    private selectedRole: string;
    private defRole: string;
    private formState: FormBehaviour = new FormBehaviour();

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private userRolesService: UserRolesSercice) {
    }

    ngOnInit() {
        this.userRoles = this.userRolesService.getRoles();
        this.defRole = this.userRoles[0].value;
        this.selectedRole = this.userRoles[0].key;

        this.userForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.maxLength(25), Validators.pattern("^[A-zА-яіІїЇєЄ.-]+$")]],
            lastName: ['', [Validators.required, Validators.maxLength(25), Validators.pattern("^[A-zА-яіІїЇєЄ.-]+$")]],
            userName: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[A-zА-яіІїЇєЄ0-9_-]+$")]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[A-z0-9_]+$")]],
            passwordConfirm: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...");
        formData.value["role"] = this.selectedRole;

        if (formData.valid) {
            this.authService.registerUser(formData).subscribe(
                x => {
                    this.formState.changeState(FormStates.success, "Користувача успішно збережено");
                },
                error => {
                    this.handleError(error);
                }
            )
        } else {
            this.handleError();
        }
    }

    handleError(error = null) {
        this.formState.changeState(FormStates.error, "Помилка");
    }

    roleSelected($event: NgbTypeaheadSelectItemEvent) {
        this.selectedRole = $event.item.key;
    }

    search = (text$: Observable<string>) =>
        text$
            .map(term => this.userRoles.filter(v => new RegExp(term, 'gi').test(v.value)));

    formatter = (x: { key: string, value: string }) => x.value;

}
