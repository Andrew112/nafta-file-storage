﻿import { Component, Input } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { AuthService } from '../../services/auth.service'
import { UserService } from '../../services/user.service'
import { UserRolesSercice } from '../../services/userRoles.service'

import { User } from '../../models/user'

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
    selector: 'user-edit',
    templateUrl: 'userEdit.component.html'
})
export class UserEditComponent {
    @Input() id: number;
    @Input() user: User;

    private userRoles: any;
    private selectedRole: string;
    private defRole: string;

    private formState: FormBehaviour = new FormBehaviour();
    private userEditForm: FormGroup;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private http: Http,
        private userRolesService: UserRolesSercice) {
    }

    ngOnInit() {
        this.userRoles = this.userRolesService.getRoles();
        this.defRole = this.userRoles[0].value;
        this.selectedRole = this.userRoles[0].key;

        this.userEditForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.maxLength(25), Validators.pattern("^[A-zА-яіІїЇєЄ.-]+$")]],
            lastName: ['', [Validators.required, Validators.maxLength(25), Validators.pattern("^[A-zА-яіІїЇєЄ.-]+$")]],
            userName: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[A-zА-яіІїЇєЄ0-9_-]+$")]],
            phoneNumber: ['', [Validators.pattern("^[0-9\+]+$")]],
        });

        this.userEditForm.patchValue(this.user);
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...")

        this.userService.updateUser(formData.value, this.id).subscribe(
            x => {
                this.formState.changeState(FormStates.success, "На вказану електронну адресу надіслано листа з подальшими інструкціями");
            },
            error => {
                this.onSubmitFailed(error);
            }
        )
    }

    private onSubmitFailed(error) {
        switch (error.status) {
            case 403:
                this.formState.changeState(FormStates.error, "Ви ввели некоректні дані");
                break;
            case 0:
                this.formState.changeState(FormStates.error, "Відсутній зв'язок з сервером");
                break;
            default:
                this.formState.changeState(FormStates.error, "Невідома помилка");
        }
    }

    roleSelected($event: NgbTypeaheadSelectItemEvent) {
        this.selectedRole = $event.item.key;
    }

    search = (text$: Observable<string>) =>
        text$
            .map(term => this.userRoles.filter(v => new RegExp(term, 'gi').test(v.value)));

    formatter = (x: { key: string, value: string }) => x.value;
}
