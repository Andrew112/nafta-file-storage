﻿import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';
import { Http } from '@angular/http';

import { CompleterService, CompleterData, RemoteData, CompleterItem } from 'ng2-completer';
import { Observable } from 'rxjs/Observable';
import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';

import { User } from '../../models/user';

import { AuthService } from '../../services/auth.service';
import { GroupService } from '../../services/group.service'

@Component({
    selector: 'group-form',
    templateUrl: 'groupForm.component.html',
    providers: [GroupService, CompleterService, FormBuilder]
})
export class GroupFormComponent {
    private groupForm: FormGroup;

    private id: number;
    private sub: any;
    private dataLoaded: boolean = false;

    private studentsDataService: RemoteData;
    private monitorDataService: RemoteData;
    private selectedStudents: User[] = [];
    private monitor: User;

    private formState: FormBehaviour;

    constructor(
        private completerService: CompleterService,
        private formBuilder: FormBuilder,
        private http: Http,
        private groupService: GroupService,
        private route: ActivatedRoute) {

        this.groupForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(4)]]
        });
    }

    ngOnInit() {
        this.formState = new FormBehaviour();
        this.monitorDataService = this.completerService.remote("api/Users/ByName/Students/", "fullName", "fullName");
        this.monitorDataService.descriptionField("userName");
        this.monitorDataService.headers(AuthService.getAuthorizationHeaders());

        this.studentsDataService = this.completerService.remote("api/Users/ByName/Students/", "fullName", "fullName");
        this.studentsDataService.descriptionField("userName");
        this.studentsDataService.headers(AuthService.getAuthorizationHeaders());

        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];    // Parsing id param as number if exists

            if (this.id) {
                this.groupService.getGroupData(this.id).subscribe(
                    x => {
                        let result = x.json();
                        this.groupForm.patchValue(result);
                        this.monitor = result.monitor;
                        this.selectedStudents = result.students;
                        this.dataLoaded = true;
                        console.log(result);
                    },
                    error => console.log(error)
                );
            }
        });
    }

    //  Is fired when user adds a student to group
    studentSelected($event) {
        if ($event) {
            //  Pushing selected student to array
            this.selectedStudents.push($event.originalObject as User);
            //  Sorting the students array
            this.selectedStudents = this.selectedStudents.sort(User.sortFunc);
        }
    }

    removeStudent(index: number) {
        this.selectedStudents.splice(index, 1);
    }

    monitorSelected($event) {
        if ($event) {
            this.monitor = $event.originalObject as User;
        }
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...");

        if (this.id)
            formData.value["id"] = this.id;
        formData.value["students"] = this.selectedStudents.map(x => x.userName);
        formData.value["monitor"] = this.monitor.userName;

        if (formData.valid) {
            if (this.id) {  // If editing
                this.groupService.putGroup(this.id, formData).subscribe(
                    x => {
                        this.formState.changeState(FormStates.success, "Успішно надіслано!");
                    },
                    error => {
                        this.handleError(error);
                    }
                )
            } else {
                this.groupService.saveGroup(formData).subscribe(
                    x => {
                        this.formState.changeState(FormStates.success, "Надіслано!");
                    },
                    error => {
                        this.handleError(error);
                    }
                )
            }
        } else {
            this.handleError();
        }
    }

    handleError(error = null) {
        this.formState.changeState(FormStates.error, "Помилка!");
    }
}
