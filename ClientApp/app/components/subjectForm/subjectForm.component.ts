﻿import { Component } from '@angular/core';
import { Http, Request, RequestOptions, Headers } from '@angular/http';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';
import { CompleterService, CompleterData, RemoteData, CompleterItem } from 'ng2-completer';

import { Observable } from 'rxjs/Observable';

import { AuthService } from '../../services/auth.service';
import { SubjectService } from '../../services/subject.service';
import { UserService } from '../../services/user.service';

import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';

import { AssignmentTemplate } from '../../models/assignmentTemplate';

@Component({
    selector: 'subject-form',
    templateUrl: 'subjectForm.component.html',
    providers: [SubjectService, UserService, FormBuilder]
})
export class SubjectFormComponent {
    private subjectForm: FormGroup;
    private uploadedFileIds: number[] = [];
    private filesPending: boolean = false;
    private formState: FormBehaviour;

    constructor(
        private http: Http,
        private subjectService: SubjectService,
        private formBuilder: FormBuilder,
        private completerService: CompleterService,
        private userService: UserService) {
    }

    ngOnInit() {
        this.formState = new FormBehaviour();
        this.subjectForm = this.formBuilder.group({
            name: [''],
            description: [''],
            assignments: this.formBuilder.array([
                this.initAssignment(),
            ])
        });
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...");

        formData.value["files"] = this.uploadedFileIds;

        if (formData.valid) {
            this.subjectService.saveSubject(formData).subscribe(
                x => {
                    this.formState.changeState(FormStates.success, "Надіслано!");
                },
                error => {
                    this.handleError(error);
                }
            )
        } else {
            this.handleError();
        }
    }

    fileChange($event) {
        this.uploadedFileIds = $event.uploadedFiles.map(x => x.id);
        this.filesPending = $event.hasPending;
    }

    initAssignment() {
        // initialize our assignments
        return this.formBuilder.group({
            name: [''],
            description: ['']
        });
    }

    addAssignment() {
        const control = <FormArray>this.subjectForm.controls['assignments'];
        control.push(this.initAssignment());
    }

    removeAssignment(index: number) {
        (<FormArray>this.subjectForm.controls['assignments']).removeAt(index);
    }
    
    handleError(error = null) {
        this.formState.changeState(FormStates.error, "Помилка!");
    }
}