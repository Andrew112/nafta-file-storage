﻿import { Component } from '@angular/core';

@Component({
    selector: 'subject-page',
    template: 'Hello my name is {{name}}.'
})
export class SubjectComponent {
    constructor() {
    }
}
