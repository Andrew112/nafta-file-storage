﻿import { Component, Input, Output, TemplateRef, EventEmitter } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

import { Observable } from "RxJS/Rx";

@Component({
    selector: 'sorting-list',
    templateUrl: 'sortingList.component.html'
})
export class SortingListComponent {
    private items: Array<any>;

    private canLoad: boolean = true;

    private offset: number = 0;
    private take: string = "25";
    private takeOptions = ["2", "10", "25", "50", "75", "100", "200"];
    private query: string;
    private sort: string = "asc";

    @Input() template: TemplateRef<any>
    @Input() restUrl: string;
    @Input() getMethod: Function;

    constructor(private http: Http) {
    }

    ngOnInit() {
        this.update();
    }

    onQueryChanged($event) {
        this.update();
    }

    onTakeChange($event) {
        this.update();
    }

    onSortChange() {
        this.sort = this.sort == "asc" ? "desc" : "asc";
        this.update();
    }

    loadMore() {
        this.offset += +this.take;
        this.getData().subscribe(
            x => {
                this.canLoad = x.length < +this.take;
                this.items = this.items.concat(x);
            }
        );
    }

    update() {
        this.reset();
        this.getData().subscribe(
            x => {
                this.canLoad = x.length < +this.take;
                this.items = x
            }
        );
    }

    reset() {
        this.canLoad = true;
        this.offset = 0;
    }

    getData() {
        let options = AuthService.getAuthorizationOptions();

        let params: URLSearchParams = new URLSearchParams();
        params.set('query', this.query);
        params.set('take', this.take);
        params.set('skip', this.offset.toString());
        params.set('sort', this.sort);


        options.search = params;

        return this.http.get(this.restUrl, options).map(response => response.json());
    }
}
