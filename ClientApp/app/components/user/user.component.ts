﻿import { Component, ViewChild } from '@angular/core';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Group } from '../../models/group';


@Component({
    selector: 'user-page',
    templateUrl: 'user.component.html',
    providers: [UserService]
})
export class UserComponent {
    @ViewChild('staticTabs') staticTabs: TabsetComponent;

    private changePassForm: FormGroup;


    private id: number;
    private sub: any;
    private user: User;
    private groups: Group[];

    constructor(
        private userService: UserService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];

            this.userService.getUserData(this.id).subscribe(
                x => {
                    let result = x.json();
                    this.user = result as User;
                    this.groups = result.groups as Group[];
                    console.log(result);
                },
                error => console.log(error)
            );
        });
    }

    selectTab(tab_id: number) {
        this.staticTabs.tabs[tab_id].active = true;
    }

    onDelete() {
        this.userService.deleteUser(this.id).subscribe(
            x => console.log("deleted"),
            error=> console.log("error")
            // Act somehow
        );
    }
}
