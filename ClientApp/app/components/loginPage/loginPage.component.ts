﻿import { AuthService } from '../../services/auth.service'
import { Component } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { FormsModule } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'login-page',
    templateUrl: 'loginPage.component.html',
    providers: [AuthService]
})
export class LoginPageComponent {
    private rememberMe: boolean = false;

    private formState: FormBehaviour;

    constructor(
        private cookieService: CookieService,
        private http: Http,
        private loginService: AuthService,
        private router: Router) {
    }

    ngOnInit() {
        this.formState = new FormBehaviour();
    }

    //  Is called when user submits login form
    async onSubmit(formData: NgForm) {
        if (formData.valid) {
            this.formState.changeState(FormStates.sent, "Виконується вхід");
            await this.loginService.login(formData.value["Login"], formData.value["Password"], this.rememberMe).subscribe(
                x => {
                    console.log(x.json());
                    this.loginService.loginAs(x.json(), this.rememberMe);
                    this.formState.changeState(FormStates.success, "Вхід виконано успішно");
                    this.router.navigate(['home']);
                },
                error => {
                    this.onLoginFailed(error);
                }
            )
        } else {
            this.formState.changeState(FormStates.error, "Ви ввели некоректні дані");
        }
    }

    private onLoginFailed(error) {
        switch (error.status) {
            case 404:
                this.formState.changeState(FormStates.error, "Користувача з таким логіном та паролем не існує");
                break;
            case 0:
                this.formState.changeState(FormStates.error, "Відсутній зв'язок з сервером");
                break;
            default:
                this.formState.changeState(FormStates.error, "Невідома помилка");
        }
        console.log("Login failed");
    }
}