﻿import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Group } from '../../models/group';
import { Room } from '../../models/room';

import { UserPermissions } from '../../helpers/userPermissions';
import { RoomService } from '../../services/room.service';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'room-page',
    templateUrl: 'room.component.html',
    providers: [RoomService]
})
export class RoomComponent {
    private id: number;
    private sub: any;
    private room: Room;
    private Math: any;

    constructor(
        private roomService: RoomService,
        private authService: AuthService,
        private route: ActivatedRoute) {
        this.Math = Math;
    }

    onAssignmentFileChange($event, index) {
        this.roomService.postAssignmentFile(index, $event.id).subscribe(
            x => console.log(x),
            error => console.log(error)
        );
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];    // Parsing id param as number if exists
            this.roomService.getRoomData(this.id).subscribe(
                x => {
                    this.room = x.json();
                    console.log(x.json());
                },
                error => console.log(error)
            );
        });
    }
}