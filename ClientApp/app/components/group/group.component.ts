﻿import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Group } from '../../models/group';
import { User } from '../../models/user';
import { Room } from '../../models/room';

import { GroupService } from '../../services/group.service';

@Component({
    selector: 'group-page',
    templateUrl: 'group.component.html',
    providers: [GroupService]
})
export class GroupComponent {
    private id: number;
    private sub: any;
    private group: Group;
    private students: User[];
    private rooms: Room[];

    constructor(
        private groupService: GroupService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];

            this.groupService.getGroupData(this.id).subscribe(
                x => {
                    let result = x.json();
                    this.group = result as Group;
                    this.students = result.students.sort(User.sortFunc) as User[];
                    this.rooms = result.rooms as Room[];
                },
                error => console.log(error)
            );
        });
    }
}