﻿import { Component, Input } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { AuthService } from '../../services/auth.service'
import { UserService } from '../../services/user.service'


import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'change-email',
    templateUrl: 'changeEmail.component.html'
})
export class ChangeEmailComponent {
    @Input() id: number;

    private formState: FormBehaviour = new FormBehaviour();
    private changeEmailForm: FormGroup;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private http: Http) {
    }

    ngOnInit() {
        this.changeEmailForm = this.formBuilder.group({
            newEmail: ['', [Validators.required]],
            newEmailRepeat: ['', [Validators.required]],
        });
    }

    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...")

        if (formData.value["newEmail"] != formData.value["newEmailRepeat"]) {
            this.formState.changeState(FormStates.error, "Електронні адреси не співпадають");
        } else {
            this.userService.changeEmail(formData.value, this.id).subscribe(
                x => {
                    this.formState.changeState(FormStates.success, "На вказану електронну адресу надіслано листа з подальшими інструкціями");
                },
                error => {
                    this.onSubmitFailed(error);
                }
            )
        }
    }

    private onSubmitFailed(error) {
        switch (error.status) {
            case 403:
                this.formState.changeState(FormStates.error, "Ви ввели некоректні дані");
                break;
            case 0:
                this.formState.changeState(FormStates.error, "Відсутній зв'язок з сервером");
                break;
            default:
                this.formState.changeState(FormStates.error, "Невідома помилка");
        }
    }
}
