﻿import { Component } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { NgForm, FormsModule, Validators, FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';

//  Autocomplete
import { CompleterService, CompleterData, RemoteData, CompleterItem } from 'ng2-completer';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';


import { Assignment } from '../../models/assignment';
import { FormBehaviour, FormStates } from '../../helpers/form-behaviour';
import { Group } from '../../models/group';
import { StoredFile } from '../../models/file';
import { Subject } from '../../models/subject';
import { User } from '../../models/user';

import { AuthService } from '../../services/auth.service';
import { RoomService } from '../../services/room.service';
import { SubjectService } from '../../services/subject.service';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'room-form',
    templateUrl: 'roomForm.component.html',
    providers: [RoomService, SubjectService, UserService, CompleterService, FormBuilder]
})
export class RoomFormComponent {
    private roomForm: FormGroup;

    private id: number;
    private sub: any;
    private dataLoaded: boolean = false;

    private subjectsDataService: RemoteData;
    private groupsDataService: RemoteData;
    private professorsDataService: RemoteData;
    private selectedProfessor: User;
    private selectedSubject: string;
    private selectedGroups: string[];
    //private nameEditable: boolean;

    private uploadedFiles: StoredFile[] = [];
    private filesPending: boolean = false;

    private formState: FormBehaviour;

    constructor(
        private completerService: CompleterService,
        private formBuilder: FormBuilder,
        private http: Http,
        private roomService: RoomService,
        private route: ActivatedRoute,
        private subjectService: SubjectService,
        private userService: UserService) {
    }

    ngOnInit() {
        this.selectedGroups = []
        this.formState = new FormBehaviour();
        //  Setting up the remote connections for ng2-completer
        //  Subjects
        this.subjectsDataService = this.completerService.remote("api/Subjects/ByName/", "name", "name");
        this.subjectsDataService.headers(AuthService.getAuthorizationHeaders());
        //  Groups
        this.groupsDataService = this.completerService.remote("api/Groups/ByName/", "name", "name");
        this.groupsDataService.headers(AuthService.getAuthorizationHeaders());
        //  Professor
        this.professorsDataService = this.completerService.remote("api/Users/ByName/Professors/", "fullName", "fullName");
        this.professorsDataService.descriptionField("userName");
        this.professorsDataService.headers(AuthService.getAuthorizationHeaders());

        this.roomForm = this.formBuilder.group({
            //name: ['', [Validators.required, Validators.pattern("^[A-zА-яіІїЇєЄ0-9() -]+$")]],
            //nameEditable: [false],
            //description: [''],
            assignments: this.formBuilder.array([])
        });

        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];    // Parsing id param as number if exists

            if (this.id) {
                this.roomService.getRoomData(this.id).subscribe(
                    x => {
                        let result = x.json();
                        for (var i = 0; i < result.assignments.length; i++) {
                            this.addAssignment()
                        }
                        this.roomForm.patchValue(result);
                        this.selectedProfessor = result.professor;
                        this.selectedGroups = result.groups.map(x => x.name);
                        this.selectedSubject = result.subject.name;
                        this.uploadedFiles = result.files;
                        this.dataLoaded = true;
                    },
                    error => console.log(error)
                );
            }
        });
    }

    initAssignment() {
        // initialize our assignments
        return this.formBuilder.group({
            id: [],
            name: ['', Validators.required],
            description: ['']
        });
    }

    addAssignment() {
        const control = <FormArray>this.roomForm.controls['assignments'];
        control.push(this.initAssignment());
    }

    fileChange($event) {
        this.uploadedFiles = $event.uploadedFiles;
        this.filesPending = $event.hasPending;
    }

    removeAssignment(index: number) {
        (<FormArray>this.roomForm.controls['assignments']).removeAt(index);
    }

    //  Saves selected professor
    private professorSelected($event: CompleterItem) {
        if ($event) {
            this.selectedProfessor = $event.originalObject;
        }
    }

    //  Adds selected group to selectedGroups array
    groupSelected($event: CompleterItem) {
        if ($event) {
            this.selectedGroups.push($event.originalObject.name);
            //this.updateName();
        }
    }

    //  Deletes group with specified name from selected
    removeGroup(groupName: string) {
        let removeIndex = this.selectedGroups.indexOf(groupName);
        this.selectedGroups.splice(removeIndex, 1);
        //this.updateName();
    }

    //  Saves selected subject
    subjectSelected($event: CompleterItem) {
        if ($event) {
            this.selectedSubject = $event.originalObject.name;
            //this.updateName();
        }
    }

    //  Event called when form is submitted
    onSubmit(formData: NgForm) {
        this.formState.changeState(FormStates.sent, "Надсилання...");

        //  Adding groups and subject names to request
        if (this.id) formData.value["id"] = this.id;
        formData.value["groups"] = this.selectedGroups;
        formData.value["subject"] = this.selectedSubject;
        formData.value["professor"] = this.selectedProfessor.userName;
        let arr: any[] = formData.value["assignments"];
        for (var i = 0; i < arr.length; i++) {
            if (!arr[i].id)
                delete arr[i].id
        }

        //  Appending uploaded files ids to formData
        formData.value["files"] = this.uploadedFiles.map(x => x.id);

        if (formData.valid) {
            let response: Observable<Response> = this.id ?
                this.roomService.putRoom(this.id, formData) // If editing
                : this.roomService.saveRoom(formData);      // If creating

            response.subscribe(
                x => this.formState.changeState(FormStates.success, "Успішно надіслано!"),
                error => this.handleError(error)
            );
        } else {
            this.handleError();
        }
    }

    //  Updates value of room name input field
    //updateName() {
    //    //  If subjects is selected and 
    //    //  edit name checkbox is not set (when name should be autogenerated)
    //    if (this.selectedSubject && !this.nameEditable) {
    //        let name: string = "";
    //        //  Adding name of subject
    //        name = this.selectedSubject;
    //        name += " ("
    //        //  Appending group names
    //        for (var i = 0; i < this.selectedGroups.length; i++) {
    //            name += (i == 0 ? "" : ", ") + this.selectedGroups[i];
    //        }
    //        name += ")"
    //        this.roomForm.controls["name"].setValue(name);
    //    }
    //}

    handleError(error = null) {
        this.formState.changeState(FormStates.error, "Помилка!");
    }

}
