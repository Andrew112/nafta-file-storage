import { Animations } from '../../animations/animation';
import { AuthService } from '../../services/auth.service';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user';


@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    animations: [Animations.slideInOut]
})
export class NavMenuComponent {
    private user: User;
    private navbarShown: boolean;
    private userName: string;
    private isAdmin: boolean;

    constructor(private authService: AuthService, private router: Router) {
        this.userName = this.authService.getUserLogin();
        this.isAdmin = this.authService.isAdmin();
    }

    ngOnInit() {
        this.authService.getCurrentUser().subscribe(
            x => this.user = x.json() as User,
            error => console.log(error)
        );
    }

    onLogoutClick() {
        this.authService.logout();
        this.router.navigate(['login']);
    }
}
