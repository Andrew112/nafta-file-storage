﻿import { Component } from "@angular/core"
import { SubjectService } from '../../services/subject.service'

import { Subject } from '../../models/subject'

@Component({
    selector: "subjects-list",
    templateUrl: "subjectsList.component.html",
    providers: [SubjectService]
})
export class SubjectsListComponent {
    public subjects: Subject[];
    public errorMessage: string;

    constructor(private subjectService: SubjectService) {
        this.getSubjects();
    }

    getSubjects() {
        this.subjectService.getSubjects().subscribe(
            subjects => this.subjects = subjects.json(),
            error => this.errorMessage = <any>error
        );
    }
}