﻿import { Component } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { GroupService } from '../../services/group.service';
import { Group } from '../../models/group';

@Component({
    selector: 'groups-list',
    templateUrl: 'groupsList.component.html',
    providers: [GroupService]
})
export class GroupsListComponent {
    public groups: Group[];

    constructor(private groupService: GroupService) {
        this.getGroups();
    }

    getGroups() {
        this.groupService.getGroups(0, 0).subscribe(
            x => {
                console.log(x.json());
                this.groups = x.json()
            },
            error => console.log("error" + error)
        );
    }
}
