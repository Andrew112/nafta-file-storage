﻿import { Component } from '@angular/core';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

import { User } from '../../models/user';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    private user: User;

    constructor(
        private authService: AuthService
    ) {
        this.authService.getCurrentUser().subscribe(
            x => this.user = x.json() as User,
            error => console.log(error)
        );
    }
}
