﻿import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm, FormsModule } from '@angular/forms';

import { ProgressHttp, Progress } from "angular-progress-http";

import { AuthService } from '../../services/auth.service';
import { StoredFile } from '../../models/file';

@Component({
    selector: 'btn-uploader',
    templateUrl: 'buttonUploader.component.html'
})
export class ButtonUploaderComponent {
    private isUploading: boolean = false;
    private uploadProgress: number;
    private fileExtension: string;

    @Input() file: StoredFile;
    @Output() changed = new EventEmitter<StoredFile>();

    constructor(private http: ProgressHttp) {
    }

    ngOnInit() {
        this.updateExtension();
    }

    onChange(event) {
        let formData: FormData = new FormData();
        formData.append("files", event.srcElement.files[0])
        this.isUploading = true;
        //  Sending the request to server
        this.http
            .withUploadProgressListener(progress => this.uploadProgress = progress.percentage)
            .post("api/Files", formData, AuthService.getAuthorizationOptions()).subscribe(
            x => {
                this.file = x.json()[0];
                this.changed.emit(this.file);
                this.updateExtension();
            },
            error => console.log(error),
            () => this.isUploading = false
        );
    }

    updateExtension() {
        let result = this.file.name.match(/\.[A-z]+$/i)[0];
        this.fileExtension = result;
    }
}
