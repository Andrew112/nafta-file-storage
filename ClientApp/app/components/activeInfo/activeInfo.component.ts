﻿import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { Room } from '../../models/room';

@Component({
    selector: 'active-info',
    templateUrl: './activeInfo.component.html',
    providers: [AuthService, UserService]
})
export class ActiveInfoComponent {
    private rooms: Room[];

    constructor(
        private authService: AuthService,
        private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getUserRooms().subscribe(
            x => {
                this.rooms = x.json();
                console.log(this.rooms);
            },
            error => console.log(error)
        );
        console.log(this.rooms);
    }
}
