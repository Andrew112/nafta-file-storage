﻿export enum FormStates {
    start, sent, success, error
}

export class FormBehaviour {
    message: string = "";
    success: boolean = false;
    state: FormStates = -1;
    submited = false;

    constructor() {
    }

    changeState(state: FormStates, message?: string) {
        this.state = state;
        if (message)
            this.message = message

        // Saving that form was submitted if new state is not start
        this.submited = (state != FormStates.start);
    }

    getAlertClass(): string {
        switch (this.state) {
            case FormStates.sent:
                return 'alert-info';
            case FormStates.success:
                return 'alert-success';
            case FormStates.error:
                return 'alert-danger';
            default:
                return '';
        }
    }
}