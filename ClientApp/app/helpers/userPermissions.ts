﻿export enum UserPermissions {
    none,
    read,
    post,
    edit
}