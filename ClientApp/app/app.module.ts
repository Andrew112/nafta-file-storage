import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { UniversalModule } from 'angular2-universal';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Ng2CompleterModule } from 'ng2-completer';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProgressHttpModule } from "angular-progress-http";
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { ModalModule, TabsModule } from 'ngx-bootstrap';

import { AppComponent } from './components/app/app.component'
import { MainLayoutComponent } from './components/mainLayout/mainLayout.component';

//  Layout elements
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { ActiveInfoComponent } from './components/activeInfo/activeInfo.component';
import { FooterComponent } from './components/footer/footer.component';

//  Subjects
import { SubjectsListComponent } from './components/subjectsList/subjectsList.component';
import { SubjectFormComponent } from './components/subjectForm/subjectForm.component';

//  Users
import { UserComponent } from './components/user/user.component';
import { UserFormComponent } from './components/userForm/userForm.component';
import { UsersListComponent } from './components/usersList/usersList.component';
import { ChangePasswordComponent } from './components/changePassword/changePassword.component';
import { ChangeEmailComponent } from './components/changeEmail/changeEmail.component';
import { UserEditComponent } from './components/userEdit/userEdit.component';

//  Groups
import { GroupComponent } from './components/group/group.component';
import { GroupsListComponent } from './components/groupsList/groupsList.component';
import { GroupFormComponent } from './components/groupForm/groupForm.component';

//  Rooms
import { RoomComponent } from './components/room/room.component';
import { RoomsListComponent } from './components/roomsList/roomsList.component';
import { RoomFormComponent } from './components/roomForm/roomForm.component';

//  Pages
import { AdminPageComponent } from './components/adminPage/adminPage.component';
import { HomeComponent } from './components/home/home.component';

//  Services
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { UserRolesSercice } from './services/userRoles.service';
import { AuthGuard } from './services/authGuard';

//  UI elements
import { ButtonUploaderComponent } from './components/buttonFileUploader/buttonUploader.component';
import { CardFormComponent } from './components/cardForm/cardForm.component';
import { FileDownloadComponent } from './components/fileDownload/fileDownload.component';
import { FileIconComponent } from './components/fileIcon/fileIcon.component';
import { MultipleFileUploaderComponent } from "./components/multipleFileUploader/multipleFileUploader.component";
import { SortingListComponent } from './components/sortingList/sortingList.component';
import { TagComponent } from './components/tag/tag.component';
import { UserDataComponent } from "./components/userData/userData.component";

import { LoginPageComponent } from './components/loginPage/loginPage.component';


@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        AdminPageComponent,
        ActiveInfoComponent,
        ButtonUploaderComponent,
        CardFormComponent,
        ChangeEmailComponent,
        ChangePasswordComponent,
        FileDownloadComponent,
        FileIconComponent,
        FooterComponent,
        GroupComponent,
        GroupFormComponent,
        GroupsListComponent,
        HomeComponent,
        LoginPageComponent,
        MainLayoutComponent,
        MultipleFileUploaderComponent,
        NavMenuComponent,
        RoomComponent,
        RoomsListComponent,
        RoomFormComponent,
        SortingListComponent,
        SubjectFormComponent,
        SubjectsListComponent,
        TagComponent,
        UserComponent,
        UserDataComponent,
        UserEditComponent,
        UserFormComponent,
        UsersListComponent
    ],
    providers: [
        AuthService,
        AuthGuard,
        CookieService,
        UserRolesSercice,
        UserService
    ],
    imports: [
        Angular2FontawesomeModule,
        FormsModule,
        Ng2CompleterModule,
        NgbModule.forRoot(),
        ProgressHttpModule,
        ReactiveFormsModule,
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.

        ModalModule.forRoot(),
        TabsModule.forRoot(),

        RouterModule.forRoot([
            { path: 'login', component: LoginPageComponent },
            {
                path: '', component: MainLayoutComponent, canActivate: [AuthGuard], children: [
                    { path: '', redirectTo: 'home', pathMatch: 'full' },

                    //  Group
                    { path: 'group/list', component: GroupsListComponent },
                    { path: 'group/new', canActivate: [AuthGuard], component: GroupFormComponent },
                    { path: 'group/edit/:id', component: GroupFormComponent },
                    { path: 'group/:id', component: GroupComponent },

                    //  Room
                    { path: 'room/list', component: RoomsListComponent },
                    { path: 'room/new', component: RoomFormComponent },
                    { path: 'room/edit/:id', component: RoomFormComponent },
                    { path: 'room/:id', component: RoomComponent },

                    //  Subject
                    //{ path: 'subject', component: SubjectsComponent },
                    { path: 'subject/list', component: SubjectsListComponent },
                    { path: 'subject/new', component: SubjectFormComponent },
                    { path: 'subject/edit/:id', component: SubjectFormComponent },

                    //  User
                    { path: 'user/list', component: UsersListComponent },
                    { path: 'user/new', component: UserFormComponent },
                    { path: 'user/edit/:id', component: UserFormComponent },
                    { path: 'user/:id', component: UserComponent },
                    
                    { path: 'admin', component: AdminPageComponent },
                    { path: 'home', component: HomeComponent },

                    { path: 'upload', component: MultipleFileUploaderComponent }
                ]
            },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
    constructor(private router: Router) {
    }
}
