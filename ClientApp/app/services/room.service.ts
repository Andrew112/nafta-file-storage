﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { NgForm } from "@angular/forms";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';

@Injectable()
export class RoomService {
    constructor(private http: Http) {
    }

    getRooms(count: number, offset: number): Observable<Response> {
        return this.http.get('api/Rooms', AuthService.getAuthorizationOptions())
    }

    getRoomById(id: number): Observable<Response> {
        let url = 'api/Rooms/' + id;
        return this.http.get(url, AuthService.getAuthorizationOptions());
    }

    getRoomData(id: number): Observable<Response> {
        let url = 'api/Rooms/GetRoomComposite/' + id;
        return this.http.get(url, AuthService.getAuthorizationOptions());
    }

    postAssignmentFile(assignmentId: number, fileId: number) {
        let url = "api/Assignments/UploadFile/" + assignmentId;
        return this.http.post(url, { fileId: fileId }, AuthService.getAuthorizationOptions());
    }

    saveRoom(formData: NgForm): Observable<Response> {
        //if (formData.valid) {
        return this.http.post('api/Rooms', formData.value, AuthService.getAuthorizationOptions());
        //} else {
        //  TODO: act somehow
        //}
    }

    putRoom(id: number, formData: NgForm): Observable<Response> {
        if (id) {
            let url = 'api/Rooms/' + id;
            return this.http.put(url, formData.value, AuthService.getAuthorizationOptions());
        } else {
            throw new Error("Неправильний Id")
        }
    }
}
