﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';

import { User } from '../models/user';

@Injectable()
export class UserService {
    constructor(private http: Http) {
    }
    
    getUsers() {
        return this.http.get("/api/Users", AuthService.getAuthorizationOptions())
    }

    getUserData(id: number) {
        let url = "/api/Users/GetUserComposite/" + id;
        return this.http.get(url, AuthService.getAuthorizationOptions())
    }

    getProfessorsByName(name: string) {
        if (name.length == 0)
            return Observable.of({});
        let url = "api/Users/Professors/ByName/" + name;
        return this.http.get(url, AuthService.getAuthorizationOptions())
    }

    getUserRooms() {
        return this.http.get("/api/Users/myData/rooms", AuthService.getAuthorizationOptions());
    }

    changePassword(data, id: number) {
        let url = "/api/Users/" + id + "/change/password";
        return this.http.put(url, data, AuthService.getAuthorizationOptions());
    }

    changeEmail(data, id: number) {
        let url = "/api/Users/" + id + "/change/email";
        return this.http.put(url, data, AuthService.getAuthorizationOptions());
    }

    updateUser(data, id: number) {
        let url = "/api/Users/" + id;
        return this.http.put(url, data, AuthService.getAuthorizationOptions());
    }

    deleteUser(id: number) {
        let url = "/api/Users/" + id;
        return this.http.delete(url, AuthService.getAuthorizationOptions());
    }
}