﻿import { Injectable } from '@angular/core';

const roles = [
    { key: "student", value: "Студент" },
    { key: "professor", value: "Викладач" },
    { key: "administrator", value: "Адміністратор" }];

@Injectable()
export class UserRolesSercice {
    getRoles() {
        return roles;
    }
}