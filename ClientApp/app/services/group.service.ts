﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { NgForm } from "@angular/forms";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';

@Injectable()
export class GroupService {
    constructor(private http: Http) {
    }

    getGroups(count: number, offset: number): Observable<Response> {
        return this.http.get('api/Groups', AuthService.getAuthorizationOptions())
    }

    getGroupData(id: number) {
        let url = 'api/Groups/GetGroupComposite/' + id
        return this.http.get(url, AuthService.getAuthorizationOptions());
    }

    getGroupById(id: number) {
        let url = 'api/Groups/' + id
        return this.http.get(url, AuthService.getAuthorizationOptions());
    }

    saveGroup(formData: NgForm) {
        if (formData.valid) {
            return this.http.post('api/Groups', formData.value, AuthService.getAuthorizationOptions());
        } else {
            // TODO: act somehow
        }
    }

    putGroup(id: number, formData: NgForm) {
        let url = "api/Groups/" + id;
        return this.http.put(url, formData.value, AuthService.getAuthorizationOptions());
    }
}
