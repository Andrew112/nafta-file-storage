﻿import { CookieService } from 'angular2-cookie/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { User } from '../models/user'

@Injectable()
export class AuthService {
    constructor(
        private cookieService: CookieService,
        private http: Http) {
    }

    getUserLogin() {
        return this.getFromSession("login");
    }

    isAdmin(): boolean {
        //console.log(this.getFromSession("isAdmin"));
        return this.getFromSession("isAdmin") == 'true';
    }

    static getToken() {
        if (!sessionStorage.getItem("access_token")) {
            let cookieService = new CookieService();
            let cookie = cookieService.get("access_token");
            if (cookie && cookie != "undefined" && cookie != undefined) {
                sessionStorage.setItem("access_token", cookie)
            }
        }
        return sessionStorage.getItem("access_token");
    }

    getFromSession(query: string) {
        if (!sessionStorage.getItem(query)) {
            let cookieService = new CookieService();
            let cookie = cookieService.get(query);
            if (cookie && cookie != "undefined" && cookie != undefined) {
                sessionStorage.setItem(query, cookie)
            }
        }
        return sessionStorage.getItem(query);
    }

    static getAuthorizationHeaders(): Headers {
        return new Headers({ 'Authorization': ('Bearer ' + AuthService.getToken()) });
    }

    static getAuthorizationOptions(): RequestOptions {
        let options = new RequestOptions();
        options.headers = AuthService.getAuthorizationHeaders();
        return options;
    }

    isAuthenticated(): boolean {
        if (this.getFromSession("access_token")) {
            return true;
        }
        return false;
    }

    getCurrentUser() {
        return this.http.get("api/Login", AuthService.getAuthorizationOptions());
    }

    logout() {
        this.cookieService.remove("access_token");
        this.cookieService.remove("login");

        sessionStorage.removeItem("access_token");
        sessionStorage.removeItem("login");
    }

    login(login: string, password: string, rememberMe: boolean): Observable<Response> {
        let post = this.http.post("api/Login", { login: login, password: password })
        post.subscribe(
            x => {
                let responseObj = x.json();
                this.loginAs(responseObj, rememberMe);
            }
        );
        return post;
    }

    loginAs(response, rememberMe: boolean) {
        if (rememberMe) {
            this.cookieService.put("access_token", response.access_token)
            this.cookieService.put("login", response.login);
            this.cookieService.put("isAdmin", response.isAdmin);
        }
        sessionStorage.setItem("access_token", response.access_token);
        sessionStorage.setItem("login", response.login);
        sessionStorage.setItem("isAdmin", response.isAdmin);
    }

    registerUser(formData: NgForm): Observable<Response> {
        if (formData.valid) {
            return this.http.post("/api/Login/Register", formData.value, AuthService.getAuthorizationOptions());
        } else {
            // TODO: act somehow
        }
    }
}