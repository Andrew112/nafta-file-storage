﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { NgForm } from "@angular/forms";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';
import { Subject } from '../models/subject';

@Injectable()
export class SubjectService {
    constructor(private http: Http) {
    }

    getSubjects(): Observable<Response> {
        return this.http.get("api/Subjects", AuthService.getAuthorizationOptions());
    }

    getSubjectsByName(query): Observable<Response> {
        if (query.length == 0)
            return Observable.of({});
        let url = "api/Subjects/ByName/" + query;
        return this.http.get(url, AuthService.getAuthorizationOptions());
    }

    saveSubject(formData: NgForm): Observable<Response> {
        if (formData.valid) {
            return this.http.post("/api/Subjects", formData.value, AuthService.getAuthorizationOptions());
        } else {
            // TODO: act somehow
        }
    }

    extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}