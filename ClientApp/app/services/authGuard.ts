﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
    private user;
    constructor(private auth: AuthService, private router: Router) {
    }

    canActivate() {
        let canActivate = this.auth.isAuthenticated();
        if (!canActivate) {
            this.router.navigate(['login']);
        }
        //console.log(this.auth.isAuthenticated());
        return canActivate;
    }
}