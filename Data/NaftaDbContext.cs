﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.ModelConfiguration;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Data
{
    public class NaftaDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<AssignmentTemplate> AssignmentTemplates { get; set; }
        //  Navigation tables
        public DbSet<RoomFile> RoomFiles { get; set; }
        public DbSet<RoomGroup> RoomGroups { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<SubjectFile> SubjectFiles { get; set; }
        public DbSet<UserAssignment> UserAssignments { get; set; }

        public NaftaDbContext (DbContextOptions options) 
            : base (options) {}

        public NaftaDbContext () : base () {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserAssignment>().HasAlternateKey(x => new { x.AssignmentID, x.UserID });
            new RoomFileMap(modelBuilder.Entity<RoomFile>());
            new RoomGroupMap(modelBuilder.Entity<RoomGroup>());
            new StudentGroupMap(modelBuilder.Entity<StudentGroup>());
            new SubjectFileMap(modelBuilder.Entity<SubjectFile>());
            new UserAssignmentMap(modelBuilder.Entity<UserAssignment>());

            new UserMap(modelBuilder.Entity<User>());
            new SubjectMap(modelBuilder.Entity<Subject>());
            new GroupMap(modelBuilder.Entity<Group>());
            base.OnModelCreating(modelBuilder);
        }
    }
}
