﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class SubjectFile
    {
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }

        public int FileId { get; set; }
        public File File { get; set; }
    }
}
