﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class Group
    {
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        // Navigation
        public User Monitor { get; set; }
        [JsonIgnore]
        public List<RoomGroup> RoomGroups { get; set; }
        [JsonIgnore]
        public List<StudentGroup> StudentGroups { get; set; }

        [NotMapped]
        public IEnumerable<User> Students
            => StudentGroups?.Select(x => x.Student);

        [NotMapped]
        public IEnumerable<Room> Rooms
        {
            get => RoomGroups?.Select(x => x.Room);
        }

    }
}
