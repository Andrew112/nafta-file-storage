﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class UserAssignment
    {
        public int AssignmentID { get; set; }
        public int UserID { get; set; }
        public int FileID { get; set; }

        public DateTimeOffset? LastUpdated { get; set; }

        // Navigation
        public User User { get; set; }
        public Assignment Assignment { get; set; }

        public File File { get; set; }
    }
}
