﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class StudentGroup
    {
        public int StudentId { get; set; }
        public User Student { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
