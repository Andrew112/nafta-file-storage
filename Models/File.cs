﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class File
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Path { get; set; }
        [Required]
        public long Size { get; set; }

        // Navigation
        public User User { get; set; }
        [JsonIgnore]
        public List<SubjectFile> SubjectFiles { get; set; }
        [JsonIgnore]
        public List<RoomFile> RoomFiles { get; set; }
        [JsonIgnore]
        public List<UserAssignment> UserAssignments { get; set; }
    }
}
