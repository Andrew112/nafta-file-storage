﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Extensions;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using NaftaFileStorage.Enums;

namespace NaftaFileStorage.Models
{
    public class Room : IEquatable<Room>
    {
        public int Id { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        // Navigation
        public User Professor { get; set; }
        [JsonIgnore]
        public List<RoomGroup> RoomGroups { get; set; }
        [JsonIgnore]
        public List<RoomFile> RoomFiles { get; set; }
        public Subject Subject { get; set; }
        public List<Assignment> Assignments { get; set; }

        [NotMapped]
        public UserPermissions Permission { get; set; }
        
        [NotMapped]
        public IEnumerable<File> Files
        {
            get => RoomFiles?.Select(x => x.File);
        }

        [NotMapped]
        public IEnumerable<Group> Groups
        {
            get => RoomGroups?.Select(x => x.Group);
        }


        public bool Equals(Room room)
        {
            return Id.Equals(room.Id);
        }
    }
}
