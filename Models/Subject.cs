﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class Subject
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        // Navigation
        public List<Room> Rooms { get; set; }
        public List<SubjectFile> SubjectFiles { get; set; }
        public List<AssignmentTemplate> AssignmentTemplates { get; set; }
    }
}
