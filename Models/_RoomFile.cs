﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class RoomFile
    {
        public int RoomId { get; set; }
        public Room Room { get; set; }

        public int FileId { get; set; }
        public File File { get; set; }
    }
}
