﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    public class Assignment
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public DateTimeOffset? DeadLine { get; set; }

        // Navigation
        public Room Room{ get; set; }
        public List<UserAssignment> UserAssignments { get; set; }

    }
}
