﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NaftaFileStorage.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace NaftaFileStorage.Models
{
    [DataContract]
    public class User : IdentityUser<int>
    {
        // Data
        [DataMember]
        public override int Id { get => base.Id; set => base.Id = value; }

        [DataMember]
        public override string UserName { get => base.UserName; set => base.UserName = value; }
        [DataMember]
        public override string Email { get => base.Email; set => base.Email = value; }
        [DataMember]
        public override string PhoneNumber { get => base.PhoneNumber; set => base.PhoneNumber = value; }

        [DataMember, Required, StringLength(50, MinimumLength = 1)]
        public string FirstName { get; set; }
        [DataMember, Required, StringLength(50, MinimumLength = 1)]
        public string LastName { get; set; }

        // Navigation
        public List<UserAssignment> UserAssignments { get; set; }

        public List<StudentGroup> StudentGroups { get; set; }
        public List<Room> TaughtRooms { get; set; }

        //
        [DataMember, NotMapped]
        public UserPermissions Permission { get; set; }

        [DataMember, NotMapped]
        public string FullName
        {
            get => FirstName + " " + LastName;
        }

        [DataMember, NotMapped]
        public IEnumerable<Group> Groups {
            get => StudentGroups?.Select(x => x.Group);
        }
    }
}
