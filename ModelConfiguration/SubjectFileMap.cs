﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class SubjectFileMap
    {
        public SubjectFileMap(EntityTypeBuilder<SubjectFile> entityBuilder)
        {
            entityBuilder
                .HasOne(x => x.Subject)
                .WithMany(s => s.SubjectFiles)
                .HasForeignKey(x => x.SubjectId);

            entityBuilder
                .HasOne(x => x.File)
                .WithMany(f => f.SubjectFiles)
                .HasForeignKey(x => x.FileId);

            entityBuilder.HasKey(x => new { x.SubjectId, x.FileId });
        }
    }
}
