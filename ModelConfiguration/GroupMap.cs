﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class GroupMap
    {
        public GroupMap(EntityTypeBuilder<Group> entityBuilder)
        {
            entityBuilder.HasAlternateKey(g => g.Name);
            entityBuilder.HasIndex(g => g.Name);
            entityBuilder
                .HasMany(g => g.StudentGroups)
                .WithOne(sg => sg.Group);
        }
    }
}
