﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class StudentGroupMap
    {
        public StudentGroupMap(EntityTypeBuilder<StudentGroup> entityBuilder)
        {
            entityBuilder
                .HasOne(x => x.Student)
                .WithMany(s => s.StudentGroups)
                .HasForeignKey(x => x.StudentId);

            entityBuilder
                .HasOne(x => x.Group)
                .WithMany(g => g.StudentGroups)
                .HasForeignKey(x => x.GroupId);

            entityBuilder.HasKey(x => new { x.StudentId, x.GroupId});
        }
    }
}
