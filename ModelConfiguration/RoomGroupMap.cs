﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class RoomGroupMap
    {
        public RoomGroupMap(EntityTypeBuilder<RoomGroup> entityBuilder)
        {
            entityBuilder
                .HasOne(x => x.Room)
                .WithMany(r => r.RoomGroups)
                .HasForeignKey(x => x.RoomId);

            entityBuilder
                .HasOne(x => x.Group)
                .WithMany(f => f.RoomGroups)
                .HasForeignKey(x => x.GroupId);

            entityBuilder.HasKey(x => new { x.RoomId, x.GroupId });
        }
    }
}
