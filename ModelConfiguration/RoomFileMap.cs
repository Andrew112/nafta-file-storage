﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class RoomFileMap
    {
        public RoomFileMap(EntityTypeBuilder<RoomFile> entityBuilder)
        {
            entityBuilder
                .HasOne(x => x.Room)
                .WithMany(r => r.RoomFiles)
                .HasForeignKey(x => x.RoomId);

            entityBuilder
                .HasOne(x => x.File)
                .WithMany(f => f.RoomFiles)
                .HasForeignKey(x => x.FileId);

            entityBuilder.HasKey(x => new { x.RoomId, x.FileId });
        }
    }
}
