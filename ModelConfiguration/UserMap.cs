﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class UserMap
    {
        public UserMap(EntityTypeBuilder<User> entityBuilder)
        {
            entityBuilder.HasAlternateKey(u => u.Email);
            entityBuilder.Property("Email").IsRequired(false);
            entityBuilder.HasAlternateKey(u => u.UserName);

            entityBuilder.HasIndex(u => u.UserName);
            entityBuilder.HasIndex(u => u.FirstName);
            entityBuilder.HasIndex(u => u.LastName);
            entityBuilder.HasIndex(u => u.Email);
        }
    }
}
