﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class SubjectMap
    {
        public SubjectMap(EntityTypeBuilder<Subject> entityBuilder)
        {
            entityBuilder.HasAlternateKey(s => s.Name);
            entityBuilder.HasIndex(s => s.Name);
        }
    }
}
