﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.ModelConfiguration
{
    public class UserAssignmentMap
    {
        public UserAssignmentMap(EntityTypeBuilder<UserAssignment> entityBuilder)
        {
            entityBuilder
                .HasOne(x => x.User)
                .WithMany(r => r.UserAssignments)
                .HasForeignKey(x => x.UserID);

            entityBuilder
                .HasOne(x => x.Assignment)
                .WithMany(f => f.UserAssignments)
                .HasForeignKey(x => x.AssignmentID);

            entityBuilder
                .HasOne(x => x.File)
                .WithMany(x=>x.UserAssignments)
                .HasForeignKey(x => x.FileID);

            entityBuilder.HasKey(x => new { x.UserID, x.AssignmentID });
        }
    }
}
