﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using NaftaFileStorage.Data;

namespace NaftaFileStorage.Migrations
{
    [DbContext(typeof(NaftaDbContext))]
    [Migration("20170320162739_ConfiguredModels")]
    partial class ConfiguredModels
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("NaftaFileStorage.Models.Assignment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("DeadLine");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int?>("RoomId");

                    b.Property<int?>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("RoomId");

                    b.HasIndex("SubjectId");

                    b.ToTable("Assignments");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.AssignmentTemplate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int?>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("SubjectId");

                    b.ToTable("AssignmentTemplates");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.File", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Path")
                        .IsRequired();

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Files");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("MonitorId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.HasIndex("MonitorId");

                    b.HasIndex("Name");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Room", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<int?>("ProfessorId");

                    b.Property<int?>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("ProfessorId");

                    b.HasIndex("SubjectId");

                    b.ToTable("Rooms");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomFile", b =>
                {
                    b.Property<int>("RoomId");

                    b.Property<int>("FileId");

                    b.HasKey("RoomId", "FileId");

                    b.HasIndex("FileId");

                    b.ToTable("RoomFiles");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomGroup", b =>
                {
                    b.Property<int>("RoomId");

                    b.Property<int>("GroupId");

                    b.HasKey("RoomId", "GroupId");

                    b.HasIndex("GroupId");

                    b.ToTable("RoomGroups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.StudentGroup", b =>
                {
                    b.Property<int>("StudentId");

                    b.Property<int>("GroupId");

                    b.HasKey("StudentId", "GroupId");

                    b.HasIndex("GroupId");

                    b.ToTable("StudentGroups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Subject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("ProfessorId");

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.HasIndex("Name");

                    b.HasIndex("ProfessorId");

                    b.ToTable("Subjects");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.SubjectFile", b =>
                {
                    b.Property<int>("SubjectId");

                    b.Property<int>("FileId");

                    b.HasKey("SubjectId", "FileId");

                    b.HasIndex("FileId");

                    b.ToTable("SubjectFiles");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(254);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("IsEmailVerified");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(6);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(15);

                    b.HasKey("Id");

                    b.HasAlternateKey("Email");


                    b.HasAlternateKey("FirstName", "LastName", "Password");

                    b.HasIndex("Email");

                    b.HasIndex("FirstName");

                    b.HasIndex("LastName");

                    b.ToTable("Users");

                    b.HasDiscriminator<string>("Discriminator").HasValue("User");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.UserAssignment", b =>
                {
                    b.Property<int>("UserID");

                    b.Property<int>("AssignmentID");

                    b.Property<int?>("FileId");

                    b.Property<DateTimeOffset?>("LastUpdated");

                    b.HasKey("UserID", "AssignmentID");

                    b.HasAlternateKey("AssignmentID", "UserID");

                    b.HasIndex("FileId");

                    b.ToTable("UserAssignments");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Professor", b =>
                {
                    b.HasBaseType("NaftaFileStorage.Models.User");


                    b.ToTable("Professor");

                    b.HasDiscriminator().HasValue("Professor");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Student", b =>
                {
                    b.HasBaseType("NaftaFileStorage.Models.User");


                    b.ToTable("Student");

                    b.HasDiscriminator().HasValue("Student");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Assignment", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Room")
                        .WithMany("Assignments")
                        .HasForeignKey("RoomId");

                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.AssignmentTemplate", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("AssignmentTemplates")
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.File", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Group", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Student", "Monitor")
                        .WithMany()
                        .HasForeignKey("MonitorId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Room", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Professor", "Professor")
                        .WithMany()
                        .HasForeignKey("ProfessorId");

                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("Rooms")
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomFile", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany("RoomFiles")
                        .HasForeignKey("FileId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Room", "Room")
                        .WithMany("RoomFiles")
                        .HasForeignKey("RoomId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomGroup", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Group", "Group")
                        .WithMany("RoomGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Room", "Room")
                        .WithMany("RoomGroups")
                        .HasForeignKey("RoomId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.StudentGroup", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Group", "Group")
                        .WithMany("StudentGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Student", "Student")
                        .WithMany("StudentGroups")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Subject", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Professor")
                        .WithMany("Subjects")
                        .HasForeignKey("ProfessorId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.SubjectFile", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany("SubjectFiles")
                        .HasForeignKey("FileId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("SubjectFiles")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.UserAssignment", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Assignment", "Assignment")
                        .WithMany("UserAssignments")
                        .HasForeignKey("AssignmentID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany()
                        .HasForeignKey("FileId");

                    b.HasOne("NaftaFileStorage.Models.User", "User")
                        .WithMany("UserAssignments")
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
