﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using NaftaFileStorage.Data;

namespace NaftaFileStorage.Migrations
{
    [DbContext(typeof(NaftaDbContext))]
    partial class NaftaDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Assignment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset?>("DeadLine");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int?>("RoomId");

                    b.HasKey("Id");

                    b.HasIndex("RoomId");

                    b.ToTable("Assignments");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.AssignmentTemplate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int?>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("SubjectId");

                    b.ToTable("AssignmentTemplates");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.File", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Path")
                        .IsRequired();

                    b.Property<long>("Size");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Files");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("MonitorId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.HasIndex("MonitorId");

                    b.HasIndex("Name");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Room", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<int?>("ProfessorId");

                    b.Property<int?>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("ProfessorId");

                    b.HasIndex("SubjectId");

                    b.ToTable("Rooms");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomFile", b =>
                {
                    b.Property<int>("RoomId");

                    b.Property<int>("FileId");

                    b.HasKey("RoomId", "FileId");

                    b.HasIndex("FileId");

                    b.ToTable("RoomFiles");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomGroup", b =>
                {
                    b.Property<int>("RoomId");

                    b.Property<int>("GroupId");

                    b.HasKey("RoomId", "GroupId");

                    b.HasIndex("GroupId");

                    b.ToTable("RoomGroups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.StudentGroup", b =>
                {
                    b.Property<int>("StudentId");

                    b.Property<int>("GroupId");

                    b.HasKey("StudentId", "GroupId");

                    b.HasIndex("GroupId");

                    b.ToTable("StudentGroups");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Subject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.HasIndex("Name");

                    b.ToTable("Subjects");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.SubjectFile", b =>
                {
                    b.Property<int>("SubjectId");

                    b.Property<int>("FileId");

                    b.HasKey("SubjectId", "FileId");

                    b.HasIndex("FileId");

                    b.ToTable("SubjectFiles");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasAlternateKey("UserName");

                    b.HasIndex("Email");

                    b.HasIndex("FirstName");

                    b.HasIndex("LastName");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("UserName");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.UserAssignment", b =>
                {
                    b.Property<int>("UserID");

                    b.Property<int>("AssignmentID");

                    b.Property<int>("FileID");

                    b.Property<DateTimeOffset?>("LastUpdated");

                    b.HasKey("UserID", "AssignmentID");

                    b.HasAlternateKey("AssignmentID", "UserID");

                    b.HasIndex("FileID");

                    b.ToTable("UserAssignments");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole<int>")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<int>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole<int>")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Assignment", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Room", "Room")
                        .WithMany("Assignments")
                        .HasForeignKey("RoomId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.AssignmentTemplate", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("AssignmentTemplates")
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.File", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Group", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User", "Monitor")
                        .WithMany()
                        .HasForeignKey("MonitorId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.Room", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.User", "Professor")
                        .WithMany("TaughtRooms")
                        .HasForeignKey("ProfessorId");

                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("Rooms")
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomFile", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany("RoomFiles")
                        .HasForeignKey("FileId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Room", "Room")
                        .WithMany("RoomFiles")
                        .HasForeignKey("RoomId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.RoomGroup", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Group", "Group")
                        .WithMany("RoomGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Room", "Room")
                        .WithMany("RoomGroups")
                        .HasForeignKey("RoomId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.StudentGroup", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Group", "Group")
                        .WithMany("StudentGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.User", "Student")
                        .WithMany("StudentGroups")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.SubjectFile", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany("SubjectFiles")
                        .HasForeignKey("FileId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.Subject", "Subject")
                        .WithMany("SubjectFiles")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NaftaFileStorage.Models.UserAssignment", b =>
                {
                    b.HasOne("NaftaFileStorage.Models.Assignment", "Assignment")
                        .WithMany("UserAssignments")
                        .HasForeignKey("AssignmentID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.File", "File")
                        .WithMany("UserAssignments")
                        .HasForeignKey("FileID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NaftaFileStorage.Models.User", "User")
                        .WithMany("UserAssignments")
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
