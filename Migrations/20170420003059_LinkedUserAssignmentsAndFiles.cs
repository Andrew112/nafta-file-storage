﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NaftaFileStorage.Migrations
{
    public partial class LinkedUserAssignmentsAndFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAssignments_Files_FileId",
                table: "UserAssignments");

            migrationBuilder.DropIndex(
                name: "IX_UserAssignments_FileId",
                table: "UserAssignments");

            migrationBuilder.RenameColumn(
                name: "FileId",
                table: "UserAssignments",
                newName: "FileID");

            migrationBuilder.AlterColumn<int>(
                name: "FileID",
                table: "UserAssignments",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserAssignments_Files_AssignmentID",
                table: "UserAssignments",
                column: "AssignmentID",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAssignments_Files_AssignmentID",
                table: "UserAssignments");

            migrationBuilder.RenameColumn(
                name: "FileID",
                table: "UserAssignments",
                newName: "FileId");

            migrationBuilder.AlterColumn<int>(
                name: "FileId",
                table: "UserAssignments",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_UserAssignments_FileId",
                table: "UserAssignments",
                column: "FileId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAssignments_Files_FileId",
                table: "UserAssignments",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
