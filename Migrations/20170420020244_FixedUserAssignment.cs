﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NaftaFileStorage.Migrations
{
    public partial class FixedUserAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAssignments_Files_AssignmentID",
                table: "UserAssignments");

            migrationBuilder.CreateIndex(
                name: "IX_UserAssignments_FileID",
                table: "UserAssignments",
                column: "FileID");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAssignments_Files_FileID",
                table: "UserAssignments",
                column: "FileID",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAssignments_Files_FileID",
                table: "UserAssignments");

            migrationBuilder.DropIndex(
                name: "IX_UserAssignments_FileID",
                table: "UserAssignments");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAssignments_Files_AssignmentID",
                table: "UserAssignments",
                column: "AssignmentID",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
