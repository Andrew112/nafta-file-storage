﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NaftaFileStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Auth
{
    public static class RolesData
    {
        public const string studentRoleName = "student";
        public const string profRoleName = "professor";
        public const string adminRoleName = "administrator";

        public static string[] Roles { get; } = new string[] { studentRoleName, profRoleName, adminRoleName };

        public static async Task SeedRoles(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<Data.NaftaDbContext>();

                if (dbContext.Database.GetPendingMigrations().Any())
                {
                    await dbContext.Database.MigrateAsync();
                }

                var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole<int>>>();
                var userManager = serviceProvider.GetRequiredService<UserManager<User>>();

                //  Creating roles
                foreach (var role in Roles)
                {
                    if (!await roleManager.RoleExistsAsync(role))
                    {
                        await roleManager.CreateAsync(new IdentityRole<int>(role));
                    }
                }

                //  Creating admin with username admin and password admin if none other exists
                var adminRole = await roleManager.FindByNameAsync(adminRoleName);
                if (!await dbContext.Users.AnyAsync(x => x.Roles.Any(y => y.RoleId == adminRole.Id)))
                {
                    var userAdmin = new User()
                    {
                        FirstName = "Admin",
                        LastName = "Admin",
                        UserName = "Admin"
                    };
                    await userManager.CreateAsync(userAdmin);
                    var chkUser = await userManager.AddPasswordAsync(userAdmin, "adminadmin");
                    await userManager.AddToRoleAsync(userAdmin, adminRoleName);

                }
            }
        }
    }
}
