﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace NaftaFileStorage.AuthServer
{
    public class AuthOptions
    {
        public const string ISSUER = "NaftaFSAuthServer"; // Token provider name
        public const string AUDIENCE = "http://localhost/"; // Token consumer
        const string KEY = "mysupersecret_secretkey!123";   // Encryption key
        public const int LIFETIME = 60 * 24 * 7; // 7 Days
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}