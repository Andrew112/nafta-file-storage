﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Auth;
using NaftaFileStorage.Data;
using NaftaFileStorage.Enums;
using NaftaFileStorage.FormModels;
using NaftaFileStorage.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Controllers
{
    //[Authorize("Bearer")]
    [Produces("application/json")]
    [Route("api/Rooms")]
    public class RoomsController : Controller
    {
        private readonly NaftaDbContext _context;
        private readonly UserManager<User> _userManager;

        public RoomsController(NaftaDbContext context,
            UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: api/Rooms
        [HttpGet]
        public IEnumerable<Room> GetRoom()
        {
            return _context.Rooms
                .Include(x => x.Subject)
                .Include(x => x.Professor)
                .Include(x => x.RoomGroups)
                    .ThenInclude(x => x.Group);
        }

        // GET: api/Rooms/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoom([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var room = await _context.Rooms
                .Include(x => x.Subject)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (room == null)
            {
                return NotFound();
            }

            return Ok(room);
        }


        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetRoomComposite([FromRoute] int id)
        {
            var room = await GetRoomCompositeInner(id);
            if (room == null)
                return NotFound();

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            room.Permission = await GetPermissionsAsync(user, room);

            if (!await _userManager.IsInRoleAsync(user, RolesData.profRoleName))
                room.Assignments.ForEach(x => x.UserAssignments.RemoveAll(y => y.UserID != user.Id));

            return Ok(room);
        }

        private async Task<Room> GetRoomCompositeInner(int id)
        {
            return await _context.Rooms
                .Where(x => x.Id == id)
                .Include(x => x.Professor)
                .Include(x => x.Subject)
                .Include(x => x.Assignments)
                    .ThenInclude(x => x.UserAssignments)
                    .ThenInclude(x => x.User)
                .Include(x => x.Assignments)
                    .ThenInclude(x => x.UserAssignments)
                    .ThenInclude(x => x.File)
                .Include(x => x.RoomFiles)
                    .ThenInclude(x => x.File)
                .Include(x => x.RoomGroups)
                    .ThenInclude(x => x.Group)
                    .ThenInclude(x => x.StudentGroups)
                    .ThenInclude(x => x.Student)
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

        [NonAction]
        private async Task<UserPermissions> GetPermissionsAsync(User user, Room room)
        {
            var roles = await _userManager.GetRolesAsync(user);
            if (roles.Contains(RolesData.adminRoleName))
                return UserPermissions.edit;

            if (roles.Contains(RolesData.studentRoleName)
                && room.Groups.Any(x => x.Students.Any(y => y.Id == user.Id)))
            {
                return UserPermissions.post;
            }
            if (user.Id == room.Professor.Id)
                return UserPermissions.read;

            return UserPermissions.none;
        }

        // PUT: api/Rooms/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoom([FromRoute] int id, [FromBody]RoomFormModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != model.Id)
                return BadRequest();
            var newRoom = model.Room;

            newRoom.Professor = _context.Users.AsNoTracking().First(x => x.UserName == model.Professor);
            newRoom.Subject = _context.Subjects.AsNoTracking().First(x => x.Name == model.Subject);
            newRoom.Assignments = model.Assignments.ToList();

            var roomFiles = _context.RoomFiles.Where(x => x.Room == newRoom).AsNoTracking().ToList();
            _context.RemoveRange(roomFiles.Where(x => !model.Files.Contains(x.FileId)));
            newRoom.RoomFiles = model.Files.Where(x => !roomFiles.Any(y => y.FileId == x)).Select(x => new RoomFile() { Room = newRoom, FileId = x }).ToList();

            var roomGroups = await _context.RoomGroups
                .Where(x => x.Room == newRoom)
                .Include(x => x.Group)
                .ToListAsync();
            var groups = _context.Groups.Where(x => model.Groups.Contains(x.Name));
            _context.RemoveRange(roomGroups.Where(x => !model.Groups.Contains(x.Group.Name)));
            newRoom.RoomGroups = groups.Where(x => !roomGroups.Any(y => y.Group == x)).Select(x => new RoomGroup() { Room = newRoom, Group = x }).ToList();

            _context.Rooms.Update(newRoom);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoomExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        public List<T> UpdateNavigation<T>(
            DbContext context,
            DbSet<T> set,
            List<T> @new,
            List<T> old) where T : class
        {
            set.RemoveRange(old);
            var result = @new;
            @new.ForEach(x => context.Entry(x).State = EntityState.Modified);
            return result;
        }

        // POST: api/Rooms
        [HttpPost]
        public async Task<IActionResult> PostRoom([FromBody]RoomFormModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            User professor = await _context.Users
                .FirstOrDefaultAsync(x => x.UserName == model.Professor);

            if (!await _userManager.IsInRoleAsync(professor, RolesData.profRoleName))
                return BadRequest("Користувач не є викладачем");

            // Checking if Professor is set
            if (professor == null)
                return BadRequest("Потрібно вказати викладача");

            // Initializing new Room object
            Room room = new Room()
            {
                Professor = professor
            };

            // Retrieving groups and subject from the database
            int[] _groupIds = await _context.Groups
                .Where(x => model.Groups.Contains(x.Name))
                .Select(x => x.Id)
                .ToArrayAsync();

            room.Subject = await _context.Subjects
                .FirstOrDefaultAsync(x => x.Name == model.Subject);

            // Checking if subject is set
            if (room.Subject == null)
                return BadRequest("Потрібно вказати дисципліну");
            // Checking if at least one group is set
            if (_groupIds == null || _groupIds.Length == 0)
                return BadRequest("Потрібно вказати групи");

            // Saving Assignments
            if (model.Assignments != null)
            {
                Parallel.ForEach(model.Assignments, (assignment) =>
                {
                    assignment.Room = room;
                });
                await _context.Assignments.AddRangeAsync(model.Assignments);
            }

            // Saving RoomFiles
            if (model.Files != null)
            {
                await _context.RoomFiles.AddRangeAsync(
                from fileId in model.Files.AsParallel()
                select new RoomFile() { FileId = fileId, Room = room }
                );
            }

            // TODO save all the stuff
            _context.Rooms.Add(room);

            // Saving references between room and groups
            Parallel.ForEach(_groupIds, (groupId) =>
            {
                RoomGroup roomGroup = new RoomGroup()
                {
                    GroupId = groupId,
                    Room = room
                };
                _context.RoomGroups.Add(roomGroup);
            });

            // Saving all changes
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetRoom", new { id = room.Id });
        }

        // DELETE: api/Rooms/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRoom([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var room = await _context.Rooms.SingleOrDefaultAsync(m => m.Id == id);
            if (room == null)
            {
                return NotFound();
            }

            _context.Rooms.Remove(room);
            await _context.SaveChangesAsync();

            return Ok(room);
        }

        private bool RoomExists(int id)
        {
            return _context.Rooms.Any(e => e.Id == id);
        }
    }
}