﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NaftaFileStorage.Auth;
using NaftaFileStorage.AuthServer;
using NaftaFileStorage.Data;
using NaftaFileStorage.FormModels;
using NaftaFileStorage.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class AuthController : Controller
    {
        private readonly NaftaDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole<int>> _roleManager;
        private readonly ILogger _logger;

        public AuthController(
            NaftaDbContext dbContext,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<IdentityRole<int>> roleManager,
            ILoggerFactory loggerFactory)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<AuthController>();
        }

        // TODO: [ValidateAntiForgeryToken]
        [HttpPost("[action]")]
        [Authorize("Bearer", Roles = RolesData.adminRoleName)]
        public async Task<IActionResult> Register([FromBody]RegisterFormModel model)
        {
            _logger.LogDebug("Registration");
            if (ModelState.IsValid)
            {
                //  Creating user object
                var user = new User
                {
                    UserName = model.UserName,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber
                };

                //  Checking if user with such login exists
                if (_dbContext.Users.Any(user_ => user_.UserName == user.UserName))
                    return BadRequest("Користувач з таким логіном вже існує");

                //  Creating user
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    // Configuring user role
                    IdentityRole<int> role = null;
                    if (RolesData.Roles.Contains(model.Role))
                        role = await _roleManager.FindByNameAsync(model.Role);
                    else
                        role = await _roleManager.FindByNameAsync(RolesData.Roles[0]);

                    IdentityUserRole<int> userRole = new IdentityUserRole<int>()
                    {
                        RoleId = role.Id,
                        UserId = user.Id
                    };

                    _dbContext.UserRoles.Add(userRole);
                    await _dbContext.SaveChangesAsync();

                    _logger.LogInformation(3, "User created a new account with password.");
                    return Ok("Користувача створено успішно!");
                }
                else
                {
                    return BadRequest(result.Errors);
                }
            }
            return BadRequest("Ви ввели некоректні дані");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginFormModel model)
        {
            //  Acquiring user with specified login from database
            var user = await _userManager.FindByNameAsync(model.Login);

            //  Error if such user doesn't exist
            if (user == null)
                return NotFound(new { message = "Користувача з такими логіном та паролем не існує" });
            //  Checking password
            bool isPasswordCorrect = await _userManager.CheckPasswordAsync(user, model.Password);
            if (!isPasswordCorrect)
                return NotFound(new { message = "Користувача з такими логіном та паролем не існує" });

            //  Adding username to claims
            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Name, user.UserName));

            //  Adding role to claims
            foreach (var role in await _userManager.GetRolesAsync(user))
                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, role));


            var now = DateTime.UtcNow;
            //  Configuring JWT token
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: await _userManager.GetClaimsAsync(user),
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            // Adding claims to user

            await _userManager.UpdateAsync(user);
            //  Configuring the response
            var response = new
            {
                access_token = encodedJwt,
                login = user.UserName,
                firstName = user.FirstName,
                lastName = user.LastName,
                isAdmin = await _userManager.IsInRoleAsync(user, RolesData.adminRoleName)
            };

            return Json(response);
        }

        [NonAction]
        private async Task<Microsoft.AspNetCore.Identity.SignInResult> SignInUserAsync(User user, string password, bool isPersistent)
        {
            await _userManager.AddClaimsAsync(
                user,
                new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.GivenName, user.FirstName),
                    new Claim(ClaimTypes.Surname, user.LastName),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.MobilePhone, user.PhoneNumber),
                });
            await _userManager.UpdateAsync(user);
            return await _signInManager.PasswordSignInAsync(user, password, isPersistent, true);
        }

        [HttpGet]
        public async Task<IActionResult> CurrentUser()
        {
            //  If user is not authorized - returning 404 
            if (User?.Identity?.Name == null)
                return NotFound();
            //  Retrieving current user's data from database
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
                return NotFound();
            else
                return Json(user);
        }
    }
}