using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/UserAssignments")]
    public class UserAssignmentsController : Controller
    {
        private readonly NaftaDbContext _context;

        public UserAssignmentsController(NaftaDbContext context)
        {
            _context = context;
        }

        // GET: api/UserAssignments
        [HttpGet]
        public IEnumerable<UserAssignment> GetUserAssignment()
        {
            return _context.UserAssignments;
        }

        // GET: api/UserAssignments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserAssignment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userAssignment = await _context.UserAssignments.SingleOrDefaultAsync(m => m.UserID == id);

            if (userAssignment == null)
            {
                return NotFound();
            }

            return Ok(userAssignment);
        }

        // PUT: api/UserAssignments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserAssignment([FromRoute] int id, [FromBody] UserAssignment userAssignment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userAssignment.UserID)
            {
                return BadRequest();
            }

            _context.Entry(userAssignment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAssignmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserAssignments
        [HttpPost]
        public async Task<IActionResult> PostUserAssignment([FromBody] UserAssignment userAssignment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserAssignments.Add(userAssignment);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserAssignmentExists(userAssignment.UserID))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUserAssignment", new { id = userAssignment.UserID }, userAssignment);
        }

        // DELETE: api/UserAssignments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAssignment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userAssignment = await _context.UserAssignments.SingleOrDefaultAsync(m => m.UserID == id);
            if (userAssignment == null)
            {
                return NotFound();
            }

            _context.UserAssignments.Remove(userAssignment);
            await _context.SaveChangesAsync();

            return Ok(userAssignment);
        }

        private bool UserAssignmentExists(int id)
        {
            return _context.UserAssignments.Any(e => e.UserID == id);
        }
    }
}