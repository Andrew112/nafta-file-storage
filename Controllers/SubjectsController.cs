﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

namespace NaftaFileStorage.Controllers
{
    [Authorize("Bearer")]
    [Produces("application/json")]
    [Route("api/Subjects")]
    public class SubjectsController : Controller
    {
        private readonly NaftaDbContext _context;

        public SubjectsController(NaftaDbContext context)
        {
            _context = context;
        }

        // GET: api/Subjects/ByName/subject
        [HttpGet("[action]/{name}")]
        public IActionResult ByName([FromRoute] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Json(_context.Subjects.Where(x => x.Name.ToLower().Contains(name.ToLower())).Take(7));
        }

        // GET: api/Subjects
        [HttpGet]
        public IEnumerable<Subject> GetSubject()
        {
            return _context.Subjects;
        }

        // GET: api/Subjects/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubject([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subject = await _context.Subjects.SingleOrDefaultAsync(m => m.Id == id);

            if (subject == null)
            {
                return NotFound();
            }

            return Ok(subject);
        }

        // PUT: api/Subjects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject([FromRoute] int id, [FromBody] Subject subject)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subject.Id)
            {
                return BadRequest();
            }

            _context.Entry(subject).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (!SubjectExists(id))
                {
                    return NotFound();
                } else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subjects
        [HttpPost]
        public async Task<IActionResult> PostSubject([FromBody] dynamic formData)
        {
            Subject subject = new Subject() { Name = formData.name, Description = formData.description };

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            int[] files = null;

            if (formData.files != null)
                files = formData.files.ToObject<int[]>();

            foreach (var assignment in formData.assignments)
            {
                await _context.AssignmentTemplates.AddAsync(new AssignmentTemplate()
                {
                    Name = assignment.name,
                    Description = assignment.description,
                    Subject = subject
                });
            }

            // Creating saving the SubjectFile instances
            // If file ids were sent and if there are not more than ten files
            if (files != null && files.Length > 0 && files.Length <= 10)
            {
                //  Cheking if all specified file ids exist in th DB
                if (await _context.Files.CountAsync(
                        x => files.Contains(x.Id)) == files.Length
                    )
                {
                    await _context.SubjectFiles.AddRangeAsync(
                    from id in files.AsParallel()
                    select new SubjectFile() { FileId = id, Subject = subject }
                    );
                } else
                {
                    return BadRequest("Некоректний запит - Надіслано неіснуючі ідентифікатори файлів");
                }
            }

            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubject", new { id = subject.Id });
        }

        // DELETE: api/Subjects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubject([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subject = await _context.Subjects.SingleOrDefaultAsync(m => m.Id == id);
            if (subject == null)
            {
                return NotFound();
            }

            _context.Subjects.Remove(subject);
            await _context.SaveChangesAsync();

            return Ok(subject);
        }

        private bool SubjectExists(int id)
        {
            return _context.Subjects.Any(e => e.Id == id);
        }
    }
}