using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;
using Microsoft.AspNetCore.Identity;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/Assignments")]
    public class AssignmentsController : Controller
    {
        private readonly NaftaDbContext _context;
        private readonly UserManager<User> _userManager;

        public AssignmentsController(
            NaftaDbContext context,
            UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: api/Assignments
        [HttpGet]
        public IEnumerable<Assignment> GetAssignments()
        {
            return _context.Assignments;
        }

        // GET: api/Assignments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAssignment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assignment = await _context.Assignments.SingleOrDefaultAsync(m => m.Id == id);

            if (assignment == null)
            {
                return NotFound();
            }

            return Ok(assignment);
        }

        // PUT: api/Assignments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssignment([FromRoute] int id, [FromBody] Assignment assignment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assignment.Id)
            {
                return BadRequest();
            }

            _context.Entry(assignment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (!AssignmentExists(id))
                {
                    return NotFound();
                } else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost("[action]/{id}")]
        public async Task<IActionResult> UploadFile([FromBody] dynamic data, [FromRoute] int id)
        {
            int fileId = data.fileId;
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            //  Check for user permissions
            if (user == null)
                return Unauthorized();

            var userAssignment = await _context.UserAssignments
                .Where(x => x.Assignment.Id == id && x.UserID == user.Id)
                .FirstOrDefaultAsync();

            var file = await _context.Files.FirstOrDefaultAsync(x => x.Id == fileId);

            if (userAssignment != null)
            {
                userAssignment.File = file;
                _context.UserAssignments.Update(userAssignment);
            } else
            {
                userAssignment = new UserAssignment()
                {
                    AssignmentID = id,
                    UserID = user.Id,
                    File = file
                };
                await _context.UserAssignments.AddAsync(userAssignment);
                    
            }
            await _context.SaveChangesAsync();
            return Ok(userAssignment);
        }

        // POST: api/Assignments
        [HttpPost]
        public async Task<IActionResult> PostAssignment([FromBody] Assignment assignment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Assignments.Add(assignment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssignment", new { id = assignment.Id }, assignment);
        }

        // DELETE: api/Assignments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAssignment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assignment = await _context.Assignments.SingleOrDefaultAsync(m => m.Id == id);
            if (assignment == null)
            {
                return NotFound();
            }

            _context.Assignments.Remove(assignment);
            await _context.SaveChangesAsync();

            return Ok(assignment);
        }

        private bool AssignmentExists(int id)
        {
            return _context.Assignments.Any(e => e.Id == id);
        }
    }
}