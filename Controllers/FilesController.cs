using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/Files")]
    public class FilesController : Controller
    {
        private readonly NaftaDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<User> _userManager;

        public FilesController(NaftaDbContext context, 
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager)
        {
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        // GET: api/Files
        [HttpGet]
        public IEnumerable<Models.File> GetFiles()
        {
            return _context.Files;
        }

        // GET: api/Files/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFile([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var file = await _context.Files.SingleOrDefaultAsync(m => m.Id == id);

            if (file == null)
            {
                return NotFound();
            }

            return Ok(file);
        }

        // PUT: api/Files/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFile([FromRoute] int id, [FromBody] Models.File file)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != file.Id)
            {
                return BadRequest();
            }

            _context.Entry(file).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (!FileExists(id))
                {
                    return NotFound();
                } else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Files
        [HttpPost]
        [Authorize("Bearer")]
        public async Task<IActionResult> PostFile(ICollection<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            // full path to file in temp location
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "FileStorage");
            List<Models.File> fileModels = new List<Models.File>(files.Count);

            var user = await _userManager.GetUserAsync(HttpContext.User);
            string prefix = "";
            foreach (var formFile in files)
            {
                //  Generating physical file name as GUID
                var fileName = Guid.NewGuid().ToString() + $".{Path.GetExtension(formFile.FileName)}";
                var file = new Models.File()
                {
                    Path = prefix + fileName,
                    Name = formFile.FileName,
                    User = user,
                    Size = formFile.Length
                };

                fileModels.Add(file);

                if (formFile.Length > 0)
                {
                    using (var stream = System.IO.File.Create(Path.Combine(filePath, fileName)))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            await _context.Files.AddRangeAsync(fileModels);
            await _context.SaveChangesAsync();

            return Json(fileModels);
        }

        // DELETE: api/Files/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFile([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var file = await _context.Files.SingleOrDefaultAsync(m => m.Id == id);
            if (file == null)
            {
                return NotFound();
            }

            _context.Files.Remove(file);
            await _context.SaveChangesAsync();

            return Ok(file);
        }

        private bool FileExists(int id)
        {
            return _context.Files.Any(e => e.Id == id);
        }
    }
}