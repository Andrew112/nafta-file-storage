using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using NaftaFileStorage.Auth;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly NaftaDbContext _context;
        RoleManager<IdentityRole<int>> _roleManager;
        private readonly UserManager<User> _userManager;

        public UsersController(
            NaftaDbContext context,
            RoleManager<IdentityRole<int>> roleManager,
            UserManager<User> userManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        // GET: api/Users/UserWithLoginExists/{id(login)}
        [HttpGet("[action]/{username}")]
        public bool UserNameExists([FromRoute] string username)
        {
            return _context.Users.Any(user => user.NormalizedUserName == username.ToUpper());
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> GetUsers(
            [FromQuery] string query,
            [FromQuery] int? skip,
            [FromQuery] int? take,
            [FromQuery] string sort)
        {
            var users = (string.IsNullOrWhiteSpace(query)
                ? _context.Users        // Serching all users if query is empty
                : QueryByName(query))   // Getting only users that fit the query otherwise
                .Skip(skip ?? 0)        // Specifying offset
                .Take(take ?? 25);      // Specifying the amount of records to take (limit)

            if (sort == "desc") //  Sorting by descending if specified
                return users.OrderByDescending(x => x.LastName).ThenBy(x => x.FirstName);

            return users.OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
                return NotFound();

            return Json(user);
        }

        // GET: api/Users/5
        [HttpGet("[action]/{id}")]
        [Authorize("Bearer")]
        public async Task<IActionResult> GetUserComposite([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _context.Users
                .Where(s => s.Id == id)
                .Include(x => x.StudentGroups)
                    .ThenInclude(x => x.Group)
                .FirstOrDefaultAsync();

            if (user == null)
                return NotFound();

            var authUser = await _userManager.FindByNameAsync(User.Identity.Name);
            if (await _userManager.IsInRoleAsync(authUser, RolesData.adminRoleName))
            {
                user.Permission = Enums.UserPermissions.admin;
            }
            else
            {
                user.Permission = user.UserName == authUser.UserName 
                    ? user.Permission = Enums.UserPermissions.edit 
                    : user.Permission = Enums.UserPermissions.read;
            }

            return Ok(user);
        }


        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] int id, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpGet("ByName/Professors/{name}")]
        public async Task<IActionResult> GetProfessorsByName([FromRoute]string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return NoContent();

            var result = await QueryByNameAndRoleAsync(name, RolesData.profRoleName);
            return Json(result);
        }

        [HttpGet("ByName/Students/{name}")]
        public async Task<IActionResult> GetStudentsByName([FromRoute]string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return NoContent();

            var result = await QueryByNameAndRoleAsync(name, RolesData.studentRoleName);
            return Json(result);
        }

        [NonAction]
        private async Task<IQueryable> QueryByNameAndRoleAsync(string name, string roleName)
        {
            IQueryable<User> query = QueryByName(name);

            var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);

            var result = query.Where(user => user.Roles.Any(ur => ur.RoleId == role.Id));
            return result;
        }

        [NonAction]
        private IQueryable<User> QueryByName(string name)
        {
            var nameParts = name.ToLower().Split(' ');
            IQueryable<User> query = null;

            if (nameParts.Length > 1)
            {
                query = _context.Users.Where(user =>
                           user.FirstName.ToLower().Contains(nameParts[0])
                        && user.LastName.ToLower().Contains(nameParts[1]));
            }
            else
            {
                query = _context.Users.Where(user =>
                           user.FirstName.ToLower().Contains(nameParts[0])
                        || user.LastName.ToLower().Contains(nameParts[0]));
            }
            return query;
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [Authorize("Bearer", Roles = RolesData.adminRoleName)]
        public async Task<IActionResult> DeleteUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            await _context.Rooms.Where(x => x.Professor == user).ForEachAsync(x => x.Professor = null);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        [HttpGet("myData/rooms")]
        [Authorize("Bearer")]
        public async Task<IEnumerable<Room>> GetUserRooms()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (await _userManager.IsInRoleAsync(user, RolesData.studentRoleName))
            {
                //  Student's rooms
                user = await _context.Users.Where(x =>
                    x.UserName == user.UserName)
                    .Include(x => x.StudentGroups)
                        .ThenInclude(x => x.Group)
                        .ThenInclude(x => x.RoomGroups)
                        .ThenInclude(x => x.Room)
                        .ThenInclude(x => x.Subject)
                    .FirstOrDefaultAsync();

                return user.Groups.SelectMany(x => x.Rooms);
            }
            else if (await _userManager.IsInRoleAsync(user, RolesData.profRoleName))
            {
                //  Professor's rooms
                return _context.Users.Where(x => x.UserName == user.UserName)
                    .Include(x => x.TaughtRooms)
                        .ThenInclude(x => x.Subject)
                    .FirstOrDefaultAsync()
                    ?.Result.TaughtRooms;
            }
            else
            {
                return new List<Room>();
            }
        }

        [HttpPut("{id}/change/password")]
        [Authorize("Bearer")]
        public async Task<IActionResult> ChangePassword([FromRoute] int id, [FromBody]dynamic data)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            //  Authorized user
            var authUser = await _userManager.FindByNameAsync(User.Identity.Name);
            if (authUser?.UserName == user?.UserName || await _userManager.IsInRoleAsync(authUser, RolesData.adminRoleName))
            {
                string newPassword = data.newPassword, oldPassword = data.oldPassword;

                IdentityResult result = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return Forbid();
            }
        }

        [HttpGet("change/email/{newEmail}")]
        [Authorize("Bearer")]
        public async Task<IActionResult> SendChangeEmailToken([FromRoute] int id, [FromRoute]dynamic newEmail)
        {
            //  Authorized user
            var user = await _userManager.FindByIdAsync(id.ToString());
            var authUser = await _userManager.GetUserAsync(User);
            string changeToken = await _userManager.GenerateChangeEmailTokenAsync(authUser, newEmail);

            if (authUser?.UserName == user?.UserName || await _userManager.IsInRoleAsync(authUser, RolesData.adminRoleName))
                return Forbid();

            // Sending an email
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Nafta FS", "naftafs@gmail.com"));
            message.To.Add(new MailboxAddress(user.FullName, newEmail));
            message.Subject = "ϳ����������� ���������� ������";

            message.Body = new TextPart("plain")
            {
                Text = $@"�����, {user.FullName} 

��� ������������ ���� ���������� ������ �������� �� ����������:

/change/email/{changeToken}/{newEmail}"//  TODO URL
            };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.friends.com", 587, false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate("test", "test");

                client.Send(message);
                client.Disconnect(true);
            }
            return Ok();
        }

        [HttpGet("change/email")]
        [Authorize("Bearer")]
        public async Task<IActionResult> ChangeEmail(dynamic data)
        {
            //  Authorized user
            var user = await _userManager.GetUserAsync(User);
            IdentityResult result = await _userManager.ChangeEmailAsync(user, data.OldPassword, data.NewPassword);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}