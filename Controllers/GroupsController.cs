﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;
using NaftaFileStorage.FormModels;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/Groups")]
    public class GroupsController : Controller
    {
        private readonly NaftaDbContext _context;

        public GroupsController(NaftaDbContext context)
        {
            _context = context;
        }

        // GET: api/Groups
        [HttpGet]
        public IEnumerable<Group> GetGroup()
        {
            return _context.Groups;
        }

        // GET api/Groups/ByName/group
        [HttpGet("[action]/{name}")]
        public IActionResult ByName([FromRoute] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Json(_context.Groups.Where(x => x.Name.ToLower().Contains(name.ToLower())).Take(7));
        }

        // GET: api/Groups/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGroup([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var @group = await _context.Groups.SingleOrDefaultAsync(m => m.Id == id);

            if (@group == null)
                return NotFound();

            return Ok(@group);
        }

        // GET: api/Groups/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetGroupComposite([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var @group = await _context.Groups
                .Include(g => g.Monitor)
                .Include(g => g.RoomGroups)
                    .ThenInclude(x => x.Room)
                .Include(g => g.StudentGroups)
                    .ThenInclude(x => x.Student)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (@group == null)
                return NotFound();

            IEnumerable<User> students = null;
            if (@group.StudentGroups != null)
                students = @group.StudentGroups.Select(g => g.Student);

            IEnumerable<Room> rooms = null;
            if (@group.RoomGroups != null)
                rooms = @group.RoomGroups.Select(g => g.Room);

            return Ok(@group);
        }

        // PUT: api/Groups/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroup([FromRoute] int id, [FromBody] GroupFormModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != model.Id)
                return BadRequest();

            var group = model.Group;
            group.Monitor = await _context.Users.FirstOrDefaultAsync(x => x.UserName == model.Monitor);

            var studentGroups = await _context.StudentGroups
                .Where(x => x.Group == group)
                .Include(x => x.Student)
                .ToListAsync();

            var students = _context.Users.Where(x => model.Students.Contains(x.UserName));
            _context.RemoveRange(studentGroups.Where(x => !model.Students.Contains(x.Student.UserName)));
            group.StudentGroups = students.Where(x => !studentGroups.Any(y => y.Student == x)).Select(x => new StudentGroup() { Group = group, Student = x }).ToList();


            _context.Groups.Update(group);
            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                } else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Groups
        [HttpPost]
        public async Task<IActionResult> PostGroup([FromBody] GroupFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @group = new Group()
            {
                Name = model.Name
            };

            //  Checking if group with such name already exists
            if (_context.Groups.Any(x => x.Name == model.Name))
                return BadRequest("Така група вже існує");

            //  Setting monitor if specified
            @group.Monitor = await _context.Users.FirstOrDefaultAsync(x => x.UserName == model.Monitor);

            if (model.Students != null)
            {
                //  Creating nav links between specified students and group
                await _context.StudentGroups.AddRangeAsync(
                    from student in _context.Users.Where(x=> model.Students.Contains(x.UserName))
                    select new StudentGroup()
                    {
                        Student = student,
                        Group = @group
                    }
                );
            }

            _context.Groups.Add(@group);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGroup", new { id = @group.Id }, group);
        }

        // DELETE: api/Groups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroup([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @group = await _context.Groups.SingleOrDefaultAsync(m => m.Id == id);
            if (@group == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(@group);
            await _context.SaveChangesAsync();

            return Ok(@group);
        }

        private bool GroupExists(int id)
        {
            return _context.Groups.Any(e => e.Id == id);
        }
    }
}