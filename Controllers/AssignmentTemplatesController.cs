using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NaftaFileStorage.Data;
using NaftaFileStorage.Models;

namespace NaftaFileStorage.Controllers
{
    [Produces("application/json")]
    [Route("api/AssignmentTemplates")]
    public class AssignmentTemplatesController : Controller
    {
        private readonly NaftaDbContext _context;

        public AssignmentTemplatesController(NaftaDbContext context)
        {
            _context = context;
        }

        // GET: api/AssignmentTemplates
        [HttpGet]
        public IEnumerable<AssignmentTemplate> GetAssignmentTemplate()
        {
            return _context.AssignmentTemplates;
        }

        // GET: api/AssignmentTemplates/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAssignmentTemplate([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assignmentTemplate = await _context.AssignmentTemplates.SingleOrDefaultAsync(m => m.Id == id);

            if (assignmentTemplate == null)
            {
                return NotFound();
            }

            return Ok(assignmentTemplate);
        }

        // PUT: api/AssignmentTemplates/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssignmentTemplate([FromRoute] int id, [FromBody] AssignmentTemplate assignmentTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assignmentTemplate.Id)
            {
                return BadRequest();
            }

            _context.Entry(assignmentTemplate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssignmentTemplateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AssignmentTemplates
        [HttpPost]
        public async Task<IActionResult> PostAssignmentTemplate([FromBody] AssignmentTemplate assignmentTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.AssignmentTemplates.Add(assignmentTemplate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssignmentTemplate", new { id = assignmentTemplate.Id }, assignmentTemplate);
        }

        // DELETE: api/AssignmentTemplates/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAssignmentTemplate([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assignmentTemplate = await _context.AssignmentTemplates.SingleOrDefaultAsync(m => m.Id == id);
            if (assignmentTemplate == null)
            {
                return NotFound();
            }

            _context.AssignmentTemplates.Remove(assignmentTemplate);
            await _context.SaveChangesAsync();

            return Ok(assignmentTemplate);
        }

        private bool AssignmentTemplateExists(int id)
        {
            return _context.AssignmentTemplates.Any(e => e.Id == id);
        }
    }
}