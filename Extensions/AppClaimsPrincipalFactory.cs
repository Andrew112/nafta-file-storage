﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NaftaFileStorage.Models;
using System.Security.Claims;
using System.Threading.Tasks;

public class AppClaimsPrincipalFactory : UserClaimsPrincipalFactory<User, IdentityRole<int>>
{
    public AppClaimsPrincipalFactory(
        UserManager<User> userManager
        , RoleManager<IdentityRole<int>> roleManager
        , IOptions<IdentityOptions> optionsAccessor)
    : base(userManager, roleManager, optionsAccessor)
    { }

    public async override Task<ClaimsPrincipal> CreateAsync(User user)
    {
        var principal = await base.CreateAsync(user);

        if (!string.IsNullOrWhiteSpace(user.FirstName))
        {
            ((ClaimsIdentity) principal.Identity).AddClaims(new[] {
                 new Claim(ClaimTypes.GivenName, user.FirstName)
            });
        }

        if (!string.IsNullOrWhiteSpace(user.LastName))
        {
            ((ClaimsIdentity) principal.Identity).AddClaims(new[] {
                 new Claim(ClaimTypes.Surname, user.LastName),
            });
        }

        return principal;
    }
}