﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace NaftaFileStorage.Extensions
{
    public static class IIdentityExtension
    {
        public static string GetFirstName(this IIdentity identity)
        {
            var principal = identity as ClaimsIdentity;
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }
            var claim = principal.FindFirst(ClaimTypes.GivenName);
            return claim?.Value;
        }

        public static string GetLastName(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;
            return claimsIdentity?.FindFirst(ClaimTypes.Surname)?.Value;
        }
    }
}
