﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaftaFileStorage.Enums
{
    public enum UserPermissions
    {
        none,
        read,
        post,
        edit,
        admin
    }
}
